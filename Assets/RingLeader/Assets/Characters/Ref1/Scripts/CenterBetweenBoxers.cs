﻿/*
 * Date Made: 10/31/17
 * Last Update: 11/1/2017
 * Original Author: Brandon Vitayanuvatti
 * Purpose: At the time of creating this script, the Referee moves around the center of the ring. But when the Referee moves to the center of the ring, the ref starts glitching 
 * out and spiining out of control. This is script is going to update the transform of the Referee's new focal point to the middle point between the two boxers.
 * Note: I attempted this code by using Vector3.Distance, but it did not give the expected result. This is to answer anyone's qustion of why I didn't use the distance call.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterBetweenBoxers : RefCombo { //I made this a child of Ref Combo so I can inherit access to the red and blue boxers withou needing to create new variables


    private Vector3 centerDistance; //This variable will store the center between the two boxers
    private Vector3 currentTransform; //Stores teh current transform of this game object
    //The following four variables are for helping find the center between the boxers
    private GameObject highestPlayerX; //This variable stores whichever boxer has the greater x-value.
    private GameObject highestPlayerZ; //This variable stores whichever boxer has the greater z-value.
    private GameObject lowestPlayerX;
    private GameObject lowestPlayerZ;

    //These variables hold the center of the x and z between the boxers. This is mainly to clean up the updateCenter() method
    private float centerX = 0;
    private float centerZ = 0;



    // Use this for initialization
    public override void Start () {
        base.Start();
        currentTransform = GetComponent<Transform>().position;
        //Blue Boxer is the default for the following two variables because the blue boxer has the larger values when the match begins.
        highestPlayerX = blueBoxer;
        highestPlayerZ = blueBoxer;
        lowestPlayerX = redBoxer;
        lowestPlayerZ = redBoxer;
	}
	
	// Update is called once per frame
	void Update () {
        updateHighLow();
        centerMath();
        updateCenter();
        transform.position = centerDistance;
    }

    void updateCenter()
    {
        centerDistance = new Vector3(centerX, currentTransform.y, centerZ); //THE Y VALUE HAS NO REASON TO BE CHANGED
    }

    void updateHighLow()
    {
        if(redBoxer.transform.position.x > blueBoxer.transform.position.x)
        {
            highestPlayerX = redBoxer;
            lowestPlayerX = blueBoxer;
        }
        else //This statement runs when the blue boxer's x-value is higher or if the x value is equal. When the values are equal, it doesn't matter which boxer is assigned.
        {
            highestPlayerX = blueBoxer;
            lowestPlayerX = redBoxer;
        }
        //Same statements as above but with HighestPlayerZ
        if (redBoxer.transform.position.z > blueBoxer.transform.position.z)
        {
            highestPlayerZ = redBoxer;
            lowestPlayerZ = blueBoxer;
        }
        else
        {
            highestPlayerZ = blueBoxer;
            lowestPlayerZ = redBoxer;
        }
    }

    void centerMath()
    {
        centerX = lowestPlayerX.transform.position.x + (highestPlayerX.transform.position.x - lowestPlayerX.transform.position.x) / 2;
        centerZ = lowestPlayerZ.transform.position.z + (highestPlayerZ.transform.position.z - lowestPlayerZ.transform.position.z) / 2;
    }
}
