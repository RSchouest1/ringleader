﻿//Author: Brandon Vitayanuvatti
//Date: 11/15/2017
//Purpose: Make the Main Camera slighlty follow the referee.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackRef : MonoBehaviour {


    private GameObject referee;
    private Vector3 refPosition;
    private float refX;//Y isn't needed since the Ref is not moving up or down.
    private float refZ;

    private GameObject cam; //This camera.
    private Vector3 originalPos;
    private Vector3 posRight;
    private Vector3 posLeft;
    private Vector3 posUp;
    private Vector3 posDown;

   // private float lerpTime;
	// Use this for initialization
	void Start () {
        referee = GameObject.FindGameObjectWithTag("Referee");
        cam = this.gameObject;
        originalPos = cam.transform.position;
        posRight = new Vector3(originalPos.x + 4.0f, originalPos.y, originalPos.z);
        posLeft = new Vector3(originalPos.x - 4.0f, originalPos.y, originalPos.z);
        posUp = new Vector3(originalPos.x, originalPos.y, originalPos.z + 4.0f);
        posDown = new Vector3(originalPos.x, originalPos.y, originalPos.z - 4.0f);
       // lerpTime = 0.0f;
    }

    // Update is called once per frame
    void Update() {
        refPosition = referee.transform.position;
        refX = refPosition.x;
        refZ = refPosition.z;
        horizontalMove();
        verticalMove();
    }
    //The ring size going vertically and horizontally is about 8, so 4 is about a quarter's way to the middle.
    public void horizontalMove()
    {
        if(refX>=4.0f)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, posRight, 0.1f + Time.deltaTime); //I'm not using originalPos for first value since camera might not be at that position.
        }
        else if (refX <= -4.0f)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, posLeft, 0.1f + Time.deltaTime);
        }
        else
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, originalPos, 0.1f + Time.deltaTime);

        }
    }

    public void verticalMove()
    {
        if (refZ >= 4.0f)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, posUp, 0.1f + Time.deltaTime); //I'm not using originalPos for first value since camera might not be at that position.
        }
        else if (refZ <= -4.0f)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, posDown, 0.1f + Time.deltaTime);
        }
        else
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, originalPos, 0.1f + Time.deltaTime);

        }
    }
}
