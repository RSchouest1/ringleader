﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature: Point subtraction for fouls. Called in combo system by referee.
//                Start & End dates 06/01/2017
//                References:
//                        Links:
//*/
//---------------------------------------------------------------------------------------------
// Date: 08/11/2017
// Author: Yi Li
// Purpose: Combine this function to the system. Press Q and 2 to use in the game.
//
//---------------------------------------------------------------------------------------------
//Author: Brandon Vitayanuvatti
//Date: 11/01/2017
//Purpose: Adding comments to understand the code better and faster since there were no comments prior.
//Date: 11/05/2017 - 11/07/2017
//Purpose: Making AddPointPlayer animate the referee. Rearranging code around IEnumerator since its needed to make the Ref point to the Boxer.
//Date: 11/13/2017
//Purpose: Fixed bug where all points would be subtracted instead of just one.
//---------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
public class AddPointPlayer1 : Combo1 {

    PointsController points;

    SeperateBoxers separate;

    private Animator refAnim;

    private RefereeStateMachine refStateM;
    private GameObject redBox; //Short for Red Boxer
    private GameObject refLookAt; //Gameobject between the two boxers that the ref always looks at by default.
    private Ref_Movement1 refMove; // Since the ref is locked to looking at center between boxers. I can't use .LooAt for pointing to a boxer. Therefore I need to briefly change his Lock_centerObj.

    private bool startEnumerator = false;
    private bool enumStarted = false; //Set true once Coroutine begins. This is to prevent it from being endlessly called.

    private void Start()
    {
        points = FindObjectOfType<PointsController>(); //This is the Points Controller script on the Referee. Not sure why the original author used FindObjectOfType instead of get component.
        separate = GetComponent<SeperateBoxers>(); //This is the Seperate Boxers script on the Referee.
        refAnim = GetComponent<Animator>(); //This will grab the Animator on the Referee.

        refStateM = GetComponent<RefereeStateMachine>();
        redBox = GameObject.FindGameObjectWithTag("RedBoxer");
        refLookAt = GameObject.FindGameObjectWithTag("RefereeLookAt");
        refMove = GetComponent<Ref_Movement1>();
    }

    public void Update() //THE COROUTINE NEEDS TO BE CALLED IN UPDATE. THE CODE AFTER WAITFORSECONDS WILL NOT BE CALLED OTHERWISE.
    {
        startPointCombo();
    }

    // Use this for initialization
    public override void DoComboResult()
    {
        enumStarted = false;
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction || GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.CornerState)
        {
            refMove.setLockCenter(redBox);
            refAnim.SetTrigger("StatePointRight");
            startEnumerator = true;//Once true, this will help start the Coroutine.
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {

    }

    private IEnumerator pointCombo()
    {
       
            yield return new WaitForSecondsRealtime (1);
            refMove.setLockCenter(refLookAt);
            points.ModPlayer1Points(-1); //Subtracts the players points if 0 or greater. 
            Debug.Log("<color=red>Player1 points -1. Current points are </color>" + points.GetPlayer1Points() + ".");
            Debug.Log("Coroutine End");
    }

    private void startPointCombo()
    {
        if (startEnumerator && !enumStarted)
        {
            StartCoroutine(pointCombo());
            enumStarted = true;
        }
    }
}
