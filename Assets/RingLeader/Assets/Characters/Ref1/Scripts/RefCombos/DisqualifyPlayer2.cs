﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: Christian Cipriano - provided functionality
//                Feature - Disqualify player based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/
//---------------------------------------------------------------------------------------------
//Author: Brandon Vitayanuvatti
//Date: 11/06/2017
//Purpose: Making Disqualify scripts animate the referee.
//11/07/2017
//Purpose: Making AddPointPlayer animate the referee. Rearranging code around IEnumerator since its needed to make the Ref point to the Boxer.
//--------------------------------------------------------------------------------------------- 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisqualifyPlayer2 : RefCombo
{

    PointsController points;
    bool finish = false;

    private Animator refAnim;

    private bool startEnumerator = false;

    private RefereeStateMachine refStateM;
    private GameObject blueBox;
    private GameObject refLookAt; //Gameobject between the two boxers that the ref always looks at by default.
    private Ref_Movement1 refMove;

    public override void Start()
    {
        base.Start();
        points = FindObjectOfType<PointsController>();
        refAnim = GetComponent<Animator>(); //Brandon: This will grab the Animator on the Referee.

        refStateM = GetComponent<RefereeStateMachine>();
        blueBox = GameObject.FindGameObjectWithTag("BlueBoxer");
        refLookAt = GameObject.FindGameObjectWithTag("RefereeLookAt");
        refMove = GetComponent<Ref_Movement1>();
    }

    public void Update() //THE COROUTINE NEEDS TO BE CALLED IN UPDATE. THE CODE AFTER WAITFORSECONDS WILL NOT BE CALLED OTHERWISE.
    {
        startDisqualifyCombo();
    }

    public override void DoComboResult()
    {
        refMove.setLockCenter(blueBox);
        refAnim.SetTrigger("StateDisqualify1"); // Brandon: The animation will trigger correctly, but the game over screen is activated before the animation can be seen.
        startEnumerator = true;
           
    }
    public override void Initilialize()
    {

    }

    private IEnumerator disqualifyCombo()
    {

        yield return new WaitForSecondsRealtime(1);
        Debug.Log("Player 2 has been disqualified.");
        //Call function to end round with Player 2 as winner.
        bool wasCommandValid = CommandManager.instance.Command_GameOver();
        if (wasCommandValid)
        {
            Debug.Log("<color=red>DisqualifyP2 was successful!</color>");
            redBoxer.transform.position = new Vector3(0f, 0f, 0f);
            blueBoxer.transform.position = blueCorner;
            finish = true;
        }
        else
        {
            Debug.Log("DisqualifyP2 was not successful");
        }
    }
    private void startDisqualifyCombo()
    {
        if (startEnumerator)
        {
            StartCoroutine(disqualifyCombo());
            startEnumerator = false;
        }
    }
}
