﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature: Point subtraction for fouls. Called in combo system by referee.
//                Start & End dates 06/01/2017
//                References:
//                        Links:
//*/
// Date: 08/11/2017
// Author: Yi Li
// Purpose: Combine this function to the system and add debug into console. Press Q and 3 to use in the game.
//---------------------------------------------------------------------------------------------
//Author: Brandon Vitayanuvatti
//Date: 11/01/2017
//Purpose: Adding comments to understand the code better and faster since there were no comments prior.
//Date: 11/05/2017 - 11/07/2017
//Purpose: Making AddPointPlayer animate the referee.Rearranging code around IEnumerator since its needed to make the Ref point to the Boxer.
//Date: 11/13/2017
//Purpose: Fixed bug where all points would be subtracted instead of just one.
//---------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
public class AddPointPlayer2 : Combo1 {

    PointsController points;
    SeperateBoxers separate;

    private Animator refAnim;

    private RefereeStateMachine refStateM;
    private GameObject blueBox; //Short for Red Boxer
    private GameObject refLookAt; //Gameobject between the two boxers that the ref always looks at by default.
    private Ref_Movement1 refMove; // Since the ref is locked to looking at center between boxers. I can't use .LooAt for pointing to a boxer. Therefore I need to briefly change his Lock_centerObj.

    private bool startEnumerator = false;
    private bool enumStarted = false; //Set true once Coroutine begins. This is to prevent it from being endlessly called.

    private void Start()
    {
        points = FindObjectOfType<PointsController>();
        separate = GetComponent<SeperateBoxers>();
        refAnim = GetComponent<Animator>(); //This will grab the Animator on the Referee.

        refStateM = GetComponent<RefereeStateMachine>();
        blueBox = GameObject.FindGameObjectWithTag("BlueBoxer");
        refLookAt = GameObject.FindGameObjectWithTag("RefereeLookAt");
        refMove = GetComponent<Ref_Movement1>();
    }

    public void Update() //THE COROUTINE NEEDS TO BE CALLED IN UPDATE. THE CODE AFTER WAITFORSECONDS WILL NOT BE CALLED OTHERWISE.
    {
        startPointCombo();
    }

    // Use this for initialization
    public override void DoComboResult()
    {
        enumStarted = false;
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction || GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.CornerState)
        {
            refMove.setLockCenter(blueBox);
            refAnim.SetTrigger("StatePointRight");
            startEnumerator = true;
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {

    }

    private IEnumerator pointCombo()
    {

        yield return new WaitForSecondsRealtime(1);
        refMove.setLockCenter(refLookAt);
        points.ModPlayer2Points(-1); //Subtracts the players points if 0 or greater. 
        Debug.Log("<color=red>Player2 points -1. Current points are </color>" + points.GetPlayer2Points() + ".");
        Debug.Log("Coroutine End");
    }

    private void startPointCombo()
    {
        if (startEnumerator && !enumStarted)
        {
            StartCoroutine(pointCombo());
            enumStarted = true;
        }
    }
}
