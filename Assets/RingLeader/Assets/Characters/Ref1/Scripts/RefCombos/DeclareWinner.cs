﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Declaring winner based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/
// Date: 08/14/2017
// Author: Yi Li
// Purpose: Base on the original script, connect the function to the system and add command check. Press Q and 4 to use in the game.
//
//Date: 11/01/2017
//Updater: Brandon Vitayanuvatti
//Purpose: Adding comments to understand the code better and faster. No changes will be made outside of comments.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeclareWinner : Combo1 {
    private PointsController points;

    private void Start()
    {
        points = FindObjectOfType<PointsController>(); //Brandon: This is the Points Controller script on the Referee
    }

    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.StandingCount)
        {
            bool validCommand = CommandManager.instance.Command_GameOver(); //Brandon: This is checking if the game is in the GameOver state
            if (validCommand)
            { //Brandon: A player needs to have more points than the other person to be declared the winner by the Referee.
                if (points.GetPlayer1Points() > points.GetPlayer2Points())
                {
                    Debug.Log("Player 1 wins!");
                }
                if (points.GetPlayer1Points() < points.GetPlayer1Points())
                {
                    Debug.Log("Player 2 wins!");
                }
                if (points.GetPlayer1Points() == points.GetPlayer2Points())
                {
                    Debug.Log("Draw.");
                }
            }
            else
            {
                Debug.Log("The winner was not declared!");
            }
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }
    public override void Initilialize()
    {

    }
}
