﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: Christian Cipriano
//                Feature - Combo system input to intialize standing counter
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/
// Date: 08/16/2017
// Author: Yi Li
// Purpose: Redo Standingcount funcitionality and connect to the system.
//
//Author: Brandon Vitayanuvatti
//Date: 11/01/2017
//Purpose: Adding comments to understand the code better and faster. Needs more than the 1 comment.
//Date: 11/07/2017
//Purpose: Adding Standing Count animation.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountInitialization : RefCombo
{
    private StandingCountController scControl;
    private StandingCountCancel scCancel; //This variable and the one above do not get  assigned on start, so they currently don't have any effect. I'm not sure what there purpose is at the moment.
    private GameState_StandingCount GSC; //This references the Game State Manager Script on the GameStateManager Object in the LoadingScene scene.
    private int okStart = 0;

    private Animator refAnim;

    public override void Start()
    {
        base.Start();
        scControl = FindObjectOfType<StandingCountController>();
        scCancel = FindObjectOfType<StandingCountCancel>();
        GSC = FindObjectOfType<GameState_StandingCount>();
        refAnim = GetComponent<Animator>();
    }

    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.StandingCount)
        {
            okStart++;
            // Only in StandingCount Game State, the referee can make this command.
            if (okStart == 1)
            {
                refAnim.SetBool("BoxerDown", true);
                Debug.Log("<color=red>The Referee started standing count!</color>");
                GSC.StartCount1 = true;
            }
            else if (okStart == 2)
            {
                refAnim.SetBool("BoxerDown", false);
                Debug.Log("<color=red>The Referee paused standing count!</color>");
                GSC.StartCount1 = false;
                okStart = 0;
            }
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {
    }
}
