﻿// Date: 08/16/2017
// Author: Yi Li
// Purpose: The method to call Focus View functionality. Press C and 2 to use this command.
// 
//Author: Brandon Vitayanuvatti
//Date:11/08/2017
//Purpose: Working on finishing Focus Mode. Including fixing bug where focus mode gives a black screen and adding a Timer Bar for Focus Mode.
//Note: To help clarify who wrote what comments, I will put my name in front of my comments.
//------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusView : RefCombo
{

    //Brandon: For some reason, the cameras needed for Focus Mode are turned off when playing the game, 
    //so I need to reference them to turn them on.
    public GameObject redCamera;
    public GameObject blueCamera;

    //Brandon: I found that if I set the TargetTexture from the start. The opening scene won't render the close-ups of the
    //boxers. So I need to set the TargetTexture after the intro scene plays.
    public RenderTexture redTargetTexture;
    public RenderTexture blueTargetTexture;

    public GameObject focusBar;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    public override void DoComboResult()
    {

        CommandManager.instance.Command_FocusView();
        redCamera.GetComponent<Camera>().enabled = true;
        blueCamera.GetComponent<Camera>().enabled = true;
        redCamera.GetComponent<Camera>().targetTexture = redTargetTexture;
        blueCamera.GetComponent<Camera>().targetTexture = blueTargetTexture;
        Debug.Log("<color=red>The referee used the Focus View!</color>");
    }

    public override void Initilialize()
    {

    }
}
