﻿//Original Author: Brandon Vitayanuvatti
//Date: 11/08/2017 - 11/09/2017
//Purpose: Creating a visual representation of how long Focus Mode lasts.
//Note: For anyone looking at this in the future. If you are unfamiliar with Sliders, 
//there is a Lynda tutorial about UI by Jesse Freeman. You can watch the Sliders section to familiarize
//yourself with them.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FocusBar : MonoBehaviour {

    public Slider focusBar;
    public bool focusActive = false;

    public float maxTime = 5.0f; //I'm basing this value off what was used in FocusMode_Panel
    //I'm making this public since this value is subject to change. 
    //If there comes a time where it needs to be private, Get and Set methods will need to be added
    //and then called in FocusMode_Panel.

    private float decrementValue = 0.0f;

    //These text variables will help display the number of illegal hits each boxer as thrown.
    public Text redTextIllegal;
    public Text blueTextIllegal;

    private Illegal_Procedure illegalP;

    void Start () {
        focusBar = this.gameObject.GetComponent<Slider>();
        focusBar.minValue = 0;
        focusBar.maxValue = maxTime;
        focusBar.value = focusBar.minValue;
        focusBar.direction = Slider.Direction.RightToLeft;
        illegalP = GameObject.FindGameObjectWithTag("Referee").GetComponent<Illegal_Procedure>();
	}

    private void Update()
    {
        emptyBar();

        if(focusActive)
            decrementValue += Time.deltaTime;

        redTextIllegal.text = illegalP.getRedIllegal().ToString();
        blueTextIllegal.text = illegalP.getBlueIllegal().ToString();
    }

    //I need the != null statements in the following methods because I was getting NullReferenceExceptions.
    public void decrementTimer()
    {
        if (focusBar != null && focusActive == true)
        {
            focusBar.value = decrementValue;
        }
    }

    public void resetTimer()
    {
            Debug.Log("Reseting Focus Bar Time!");
            focusBar.value = focusBar.minValue;
            decrementValue = 0.0f;
    }

    public void emptyBar()
    {
        if(focusBar.value >= maxTime)
        {
            focusActive = false;
        }
    }

}
