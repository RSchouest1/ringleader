﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-12-2017
// Credit: 

// Purpose: Command State for the Referee

//------------------------------------------------------------------------------------------------
//
//Date: 11/04/2017
//Author: Brandon Vitayanuvatti
//Purpose: Adding Debug.Logs to see when a state triggers. Adding Animation for Separate. Inlcuding reference to Ref's animator and setting the Trigger variable.
//Note: To help clarify who wrote what comments, I will put my name in front of my comments.
//
//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_Command : RefereeState
{
    //List of available commands to the referee
    //Stop, Box, Break, Time

    //Right now lets just get Break

    //Brandon: The following two variables are needed to change the Referee's current animation.
    private GameObject referee;
    private Animator refAnimator;

    private Separate sScript; //Brandon: sScript is short for Sepearate Script. NOT SEPARATE BOXERS SCRIPT!

    public override void Initialize()
    {
        base.Initialize();
        
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=red>Command State Activated</color>");
        referee = GameObject.FindGameObjectWithTag("Referee");
        refAnimator = referee.GetComponent<Animator>();
        sScript = GameObject.Find("Separate").GetComponent<Separate>();//Brandon: Since the script that checks if boxers are close enough isn't attached to the Ref, I need to find by name.
        isSeparating();


    }

    public override void Execute() //Brandon: For whatever reason, I get an error when trying to SetTrigger in Execute. Which is why its in Enter.
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
       // refAnimator.ResetTrigger("StateSeparate");
    }


    public void isSeparating() 
    {
        if (sScript.getCanSeparate())//Brandon: If true, play separate animation
        {
            refAnimator.SetTrigger("StateSeparate"); //Brandon: For those unfamiliar with Triggers, they will automatically get set to false once the animation has played.
            sScript.setCanSeparate(false); 
        }
    }
}
