﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-18-2017
// Credit: 

// Purpose: Script for the referee state of Bout End - the state of the referee after the fight ends

//------------------------------------------------------------------------------------------------
//
//Date: 11/04/2017
//Author: Brandon Vitayanuvatti
//Purpose: Adding Debug.Logs to see when a state triggers.
//Note: To help clarify who wrote what comments, I will put my name in front of my comments.
//
//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_BoutEnd : RefereeState
{

    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=red>BoutEnd State Activated</color>");
        //gSM.ChangeGameState(Constants.GameState.GameOver);
    }
}
