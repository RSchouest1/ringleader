﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-12-2017
// Credit: 

// Purpose: Command State for the Referee

//------------------------------------------------------------------------------------------------
//
//Date: 11/04/2017
//Author: Brandon Vitayanuvatti
//Purpose: Adding Debug.Logs to see when a state triggers.
//Note: To distinguish myself from the original author, I will put my name in front of my comments.
//
//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_Paused : RefereeState
{
    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=red>Pause State Activated</color>");

    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
