﻿//------------------------------------------------------------------------------------------------

// Author: Lei Qian
// Date: 8-13-2017
// Credit: 

// Purpose: Script for the referee state of Focus - Special Referee action

//------------------------------------------------------------------------------------------------
//Author: Brandon Vitayanuvatti
//Date:11/08/2017
//Purpose:Adding on a toggle to Lei Qian's Execute move back and forth between boxers. Also making this script turn on Focus Bar.
//Note: To help clarify who wrote what comments, I will put my name in front of my comments.
//------------------------------------------------------------------------------------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_Focus : RefereeState
{

    public float distancePerFrame;

    private bool toggle = true; //Brandon: If false then look at red, if true look at blue.
    private GameObject focusTimer;
    private FocusBar focusBarScript;

    //Brandon: Because I can't find a game object that has this script attached, I can't assign public variables.
    //And since focusBar won't assign as a private variable for some reason, I need to assign by referencing another
    //script where I can assign it as a public variable. (In this case the FocusView script on the Referee).
    private FocusView refFocusView;

    //public bool checking = false;

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=red>Focus State Activated</color>");
        Debug.Log("check if im in focus mode");
        cMN.ResetTimefocusview();
        cMN.Focus.SetActive(true);

        GetMachine<RefereeStateMachine>().checking = true;

        refFocusView = GameObject.FindGameObjectWithTag("Referee").GetComponent<FocusView>();
        focusTimer = refFocusView.focusBar;
        focusBarScript = focusTimer.GetComponent<FocusBar>();
        focusTimer.SetActive(true); //Brandon: I had trouble setting this in FocusMode_Panel, so I'm turning it on here.  The problem was that it would still turn on during cool-down.
        focusBarScript.resetTimer();
        focusBarScript.focusActive = true;
    }

    public override void Execute()
    {
        base.Execute();

        if (Input.GetKeyDown(KeyCode.B) || Input.GetButtonDown("Ref_Controller_Focus"))
        {
            if (toggle)
            {
                cMN.Focus.SetActive(false);
                cMN.FocusRed.SetActive(true);
            }
           else if (!toggle)
            {
                cMN.FocusRed.SetActive(false);
                cMN.Focus.SetActive(true);
            }
            toggle = !toggle;
        }
        if (cMN.checkTimer > 5)
        {
            machine.ChangeState<RefereeState_Paused>();

        }

    }

    public override void Exit()
    {
        base.Exit();

        GetMachine<RefereeStateMachine>().checking = false;
       cMN.Focus.SetActive(false);
       cMN.FocusRed.SetActive(false);
        focusTimer.SetActive(false);
        
    }

}

