﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//
//Date: 11/04/2017
//Author: Brandon Vitayanuvatti
//Purpose: Adding Debug.Logs to see when a state triggers.
//------------------------------------------------------------------------------------------------

public class RefereeState_Idle : RefereeState
{

    public override void Enter()
    {
        base.Enter();
        Debug.Log("<color=red>IdleState Activated</color>");


    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
