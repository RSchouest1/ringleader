﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-18-2017
// Credit: 

// Purpose: Script for the referee state of KO Count - The state the referee is in when a boxer is down

//------------------------------------------------------------------------------------------------
//
//Date: 11/03/2017
//Updater: Brandon Vitayanuvatti
//Purpose: Adding comments and Debug logs to see what triggers states to understand the code better and faster. 
//Will also improve functionality of the State Machine classes
//-------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_KOCount : RefereeState
{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter() //Brandon: This triggers when a player is knocked down, not when E4 is pressed
    {
        base.Enter();
        Debug.Log("<color=red>KOCount State Activated</color>");
        gSM.ChangeGameState(Constants.GameState.StandingCount);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
