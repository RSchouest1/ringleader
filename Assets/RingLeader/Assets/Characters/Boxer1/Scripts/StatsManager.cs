﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Jordan Ferris
// Date: 10/18/17
// Purpose: To create a data system that tracks information for the boxers during the fight
// Tracks punches landed/thrown for each type of punch and total punches
// Tracks knockdowns
// Creates percentages to read this data easier
// Needs to be expanded upon to gather more information and able to be used by judges after the fight

// Worked on by Alex Ault - added all neccessary bits for tracking illegal hits

//Contributor: Brandon Vitayanuvatti
//Date: 11/14/2017
//Purpose: Add a variable reference to Illegal Procedure so that the Referee can recognize an Illegal Hit

//Contributor: Hector Castelli Zacharias
//Date: 11/15/2017
//Purpose: Added method to clear the stats and reset a match.

public class StatsManager : MonoBehaviour {

	#region Variables
	// stores total punches thrown/landed for each type of punch
	public float totalPunchesLanded;	
	public float totalPunchesThrown;	
	public float jabsLanded;			
	public float jabsThrown;			
	public float crossesLanded;
	public float crossesThrown;
	public float hooksLanded;
	public float hooksThrown;
	public float uppercutsLanded;
	public float uppercutsThrown;
	public float knockdowns;	// times knocked down
    public float illegalPunchesLanded;

	// stores percentages by dividing punches landed by punches thrown
	public float totalPunchesPercent;	
	public float jabsPercent;			
	public float crossesPercent;		
	public float hooksPercent;			
	public float uppercutsPercent;		
	#endregion

	private BoxerData1 myBoxer;		// reference to the boxer this script is attached to
    private Illegal_Procedure refIP;

	// Use this for initialization
	void Start () {

		myBoxer = GetComponent<BoxerData1> ();
        refIP = GameObject.FindGameObjectWithTag("Referee").GetComponent<Illegal_Procedure>();
	}

	// just using for testing purposes so that percentages can be read in the inspector - can remove when not needed
	void Update(){

		CreatePercentages ();
	}

	#region PunchFunctions
	public void CrossThrown(){

		crossesThrown++;
		totalPunchesThrown++;
	}

	public void CrossLanded(){

		crossesLanded++;
	}

	public void JabThrown(){

		jabsThrown++;
		totalPunchesThrown++;
	}

	public void JabLanded(){

		jabsLanded++;
	}

	public void HookThrown(){

		hooksThrown++;
		totalPunchesThrown++;
	}

	public void HookLanded(){

		hooksLanded++;
	}

	public void UppercutThrown(){

		uppercutsThrown++;
		totalPunchesThrown++;
	}

	public void UppercutLanded(){

		uppercutsLanded++;
	}

	public void PunchLanded(){

		totalPunchesLanded++;
	}

	public void Knockdown(){

		knockdowns++;
	}

    public void IllegalPunchHit()
    {
        illegalPunchesLanded++;
        refIP.illegalHitThrown();
        if(gameObject.tag == "RedBoxer")
        {
            refIP.illegalHitRed();
        }
        else
        {
            refIP.illegalHitBlue();
        }
		//print (this.gameObject + "illegalHitScore " + illegalPunchesLanded);
    }
	#endregion

	// prints all of the punching stats - can be moved to a menu later
	public void ShowBoxerStats(){

		CreatePercentages ();


		// total punches
		print (myBoxer.name + "'s total punches thrown: " + totalPunchesThrown);
		print (myBoxer.name + "'s total punches landed: " + totalPunchesLanded);
		print ("%: " + totalPunchesPercent);

		// jabs
		print (myBoxer.name + "'s total jabs thrown: " + jabsThrown);
		print (myBoxer.name + "'s total jabs landed: " + jabsLanded);
		print ("%: " + jabsPercent);

		// crosses
		print (myBoxer.name + "'s total crosses thrown: " + crossesThrown);
		print (myBoxer.name + "'s total crosses landed: " + crossesLanded);
		print ("%: " + crossesPercent);

		// hooks
		print (myBoxer.name + "'s total hooks thrown: " + hooksThrown);
		print (myBoxer.name + "'s total hooks landed: " + hooksLanded);
		print ("%: " + hooksPercent);

		// uppercuts
		print (myBoxer.name + "'s total uppercuts thrown: " + uppercutsThrown);
		print (myBoxer.name + "'s total uppercuts landed: " + uppercutsLanded);
		print ("%: " + uppercutsPercent);

		// knockdowns
		print (myBoxer.name + "'s total knockdowns: " + knockdowns);

        //Illegal punches
        print(myBoxer.name + "'s total illegal hits landed: " + illegalPunchesLanded);


	}

	// creates percentages to show how many of the boxer's punches landed out of the total that were thrown
	void CreatePercentages(){

		totalPunchesPercent = (totalPunchesLanded / totalPunchesThrown) * 100f;

		jabsPercent = (jabsLanded / jabsThrown) * 100f;

		crossesPercent = (crossesLanded / crossesThrown) * 100f;

		hooksPercent = (hooksLanded / hooksThrown) * 100f;

		uppercutsPercent = (uppercutsLanded / uppercutsThrown) * 100f;
	}

    public void ResetStats()
    {

        totalPunchesLanded =0;
        totalPunchesThrown = 0;
        jabsLanded = 0;
        jabsThrown = 0;
        crossesLanded = 0;
        crossesThrown = 0;
        hooksLanded = 0;
        hooksThrown = 0;
        uppercutsLanded = 0;
        uppercutsThrown = 0;
        knockdowns = 0;
        illegalPunchesLanded = 0;
    }
}
