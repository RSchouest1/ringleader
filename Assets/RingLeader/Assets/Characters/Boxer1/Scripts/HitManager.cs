﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Daniel Vanallen
// Contribution: Added HitReaction and disabled KOP adjustment if blocking proper hit
//    Feature: Blocking System & Hit Reaction Animation system
//    10/23/2017 - 11/19/2017
//    References: None
//    Links: Refer "void HitReaction"
//    
// Adrian Szurek
// Contribution: Added a sound manager call to get the sound manager script.
//               Added a call in hit reaction to play the punch sound.


public class HitManager : MonoBehaviour {
	public static HitManager instance;

	//accesses ANY_KOP
    public ANY_KOP KOPManager;
	//name of attacker
	private string attackerName;
	//name of defender
	private string targetName;
	//attacker game object
	private GameObject attackerGO;
	//defender game object
	private GameObject targetGO;
	//defenders animator
	Animator targetAnim;
	//bool for if hit was blocked
	bool hitBlocked;
	//attacker BoxerStateMachine
	BoxerStateMachine1 aBSM;
	//Accesses Attacker Stats Manager
	StatsManager aSM;
    //Accesses Boxer Sound Manger
    SoundManager aSoundM;
	void Awake(){
		instance = this;
	}
	//gets boxer that threw punch and boxer that took punch with damage from glove from BoxerData1
	public void hitLanded(BoxerData1 attacker, BoxerData1 target, float _TKOPfromGlove){
		//presets hitblocked before checks
		hitBlocked = false;
		//sets who is attacker from boxerData1
		attackerName = attacker.gameObject.name;
		//sets who is defender from BoxerData1
		targetName = target.gameObject.name;
		//sets attacker game object based off attackername
		attackerGO = GameObject.Find (attackerName);
		//sets defender as game object based off target name
		targetGO = GameObject.Find (targetName);
		//sets defender animator once above sets have been made
		targetAnim = targetGO.GetComponent<Animator> ();
		//sets attacker boxer state machine depending on settings made above
		aBSM = attackerGO.GetComponent<BoxerStateMachine1> ();
		//Accesses proper stats manager depending on who is attacker.
		aSM = attackerGO.GetComponent<StatsManager> ();
        //Accesess the sound manager of the target boxer
        aSoundM = attackerGO.GetComponent<SoundManager>();
		//debug to make sure settings were completed properly
		Debug.Log (attackerName + " hit "+ targetName);
		//performs hit reaction animation in this script below
		HitReactionAnimation ();
		//performs hit reaction in this script below
		HitReaction ();
		//If hit was not blocked (check setting in HitReaction()
		if (!hitBlocked) 
		{
			//sends in full damage input to ANY_KOP script
			KOPManager = target.GetComponent<ANY_KOP>();
			SetKop (_TKOPfromGlove);
			KOPManager.RandomKDCheck();
		}
	}
    public void SetKop(float _TKOP)
    {
        KOPManager.ModKOP(_TKOP);
    }
	//determines if hit was blocked
	void HitReaction ()
	{
        //Plays sound from target that is hit
        aSoundM.PlayOnHitSound();
		//If the attacker is not in dirty punch mode.
		if(!aBSM.dirtyPunchMode)
		{
			bool headBlock;
			bool bodyBlock;
			//sets head block based off BoxerData1
			headBlock = targetGO.GetComponent<BoxerData1>().isBlockEnabled;
			//sets body block based off BoxerData1
			bodyBlock = targetGO.GetComponent<BoxerData1> ().isBodyBlockEnabled;
			//Checks for successful head block
			if(headBlock)
			{
				//If attacker wasn't crouching when throwing hit it requires a head block
				if(!aBSM.isCrouching && (aBSM.getDidJab ()||aBSM.getDidLeftHook ()||aBSM.getDidLeftUppercut ()
					||aBSM.getDidRightHook ()||aBSM.getDidRightUppercut ()))
				{
					//If headblock worked set hitBlocked to true for HitLanded reaction
					hitBlocked = true;
				}
			}
			//checks for successful body block
			else if (bodyBlock)
			{
				//if attacker is crouching when throwing hit it requires a body block
				if(aBSM.isCrouching && (aBSM.getDidJab ()||aBSM.getDidLeftHook ()||aBSM.getDidLeftUppercut ()
					||aBSM.getDidRightHook ()||aBSM.getDidRightUppercut ()))
				{
					//if bodyblock worked set hitBlocked to true for Hit Landed reaction
					hitBlocked = true;
				}
			}
		}
		//if the attacker is in dirty punch mode and hits
		else if(aBSM.dirtyPunchMode)
		{
			//this function is in the StatsManager and increases the illegal hit point system by 1
			aSM.IllegalPunchHit ();
		}
	}
		
	public void HitReactionAnimation ()//this runs the animations to react to a punch
	{
		
		switch (aBSM.currentPC)//switches reactions depending on which hit was thrown
		{
		case 1://head jab
			targetAnim.SetInteger ("HitReaction", 1);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 2://head cross
			targetAnim.SetInteger ("HitReaction", 1);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 41://left hook
			targetAnim.SetInteger ("HitReaction", 2);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 42://right hook
			targetAnim.SetInteger ("HitReaction", 3);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 51://left uppercut
			targetAnim.SetInteger ("HitReaction", 4);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 52://right uppercut
			targetAnim.SetInteger ("HitReaction", 4);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 61://body jab
			targetAnim.SetInteger ("HitReaction", 5);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 62://body cross
			targetAnim.SetInteger ("HitReaction", 5);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 651://left uppercut from crouch
			targetAnim.SetInteger ("HitReaction", 5);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 652://riht upper cut from crouch
			targetAnim.SetInteger ("HitReaction", 5);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 641://left hook
			targetAnim.SetInteger ("HitReaction", 6);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		case 642://right hook
			targetAnim.SetInteger ("HitReaction", 7);
			StartCoroutine (ResetHitAnimation ());
			aBSM.dirtyPunchSetup = false;
			break;
		}

	}
	//Once any of the above cases have been performed this happens to reset the Animator to 0 (idle)
	IEnumerator ResetHitAnimation ()
	{
		//Wait this long in order for the animator to react to the change
		yield return new WaitForSeconds (.2f);
		//Reset so animation doesn't continue once performed
		targetAnim.SetInteger ("HitReaction",0);
	}
}
