﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 8/14/2017
// Cheng-An Lin
// Now the SetIsTriggerChecking function also takes two parameters to send to HitManager.
// The int is to replace bool because animationevent cannot take bool parameter.
// The float is the TKOP of the punch. To modify it, goes to animation clips to edit the event float.

// 10/13/17
// Jordan Ferris
// There is now a reference to the StatsManager script
// The hit detection now also checks what kind of punch was landed and increments that in the StatsManager script
// The SetPunch function is passed a string from the corresponding BoxerState that was used for the punch

public class Glove : MonoBehaviour {
	public int enemyLayer;
	BoxerStateMachine1 myBoxer;
    int isTriggerChecking = 0;
    float tTKOP;
	static string punch;	// used to determine which punch was thrown

	StatsManager myStats;

	// Use this for initialization
	void Start () {
		
		myBoxer = GetComponentInParent<BoxerStateMachine1> ();

		myStats = GetComponentInParent<StatsManager> ();
    }

	void OnTriggerStay(Collider collision){

        if (isTriggerChecking == 1)
        {
            if (collision.gameObject.layer == enemyLayer)
            {						
                isTriggerChecking = 0;
                BoxerData1 enemy = myBoxer.boxerData.enemyObj.GetComponent<BoxerData1>();
                HitManager.instance.hitLanded(myBoxer.boxerData, enemy , tTKOP);

				myStats.PunchLanded ();		// increments punches landed in StatsManager script

				// depending on which punch was thrown, count that as a punch landed in StatsManager
				switch(punch){

				case "cross":
					myStats.CrossLanded ();
					break;
				case "jab":
					myStats.JabLanded ();
					break;
				case "hook":
					myStats.HookLanded ();
					break;
				case "uppercut":
					myStats.UppercutLanded ();
					break;
				default:
					break;
				}
            }
        }
	}

    public void SetIsTriggerChecking(int istrue, float TKOPfromAni)
    {
        isTriggerChecking = istrue;
        tTKOP = TKOPfromAni;
    }

	// sets the punch string to the same string sent in by the corresponding BoxerState
	public void SetPunch(string punchType){

		punch = punchType;
	}
}
