﻿/*------------------------------------------------------------------------------------------------
 Original Author: Adrian Szurek
 Date: 12/1/2017

 Purpose: To setup a system to allow for sounds to played from this script and handle any 
 modifier that adjusts any sound in the game. This allows sounds to be placed in here from the 
 inspector. All punch sounds are currently handled with the same sound that adjusted in pitch
 based on the speed of the punch that is also calculated in this script. Pitch Overrides can
 be called to set the pitch on a sound by calling a float value on call on certin audio calls.

 Note: Find gloves may need to be adjusted based on models and may need to be improved.
 Also based on animation and animation speed the values of pitch may need to be adjusted.
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //sound that plays on hit
    public AudioClip hitSound;
    //sound that plays on knock down
    public AudioClip KnockDownSound;
    //sound that plays when a punch is thrown
    public AudioClip ThrowPunchSound;
    //Audio source for hit sound 
    private AudioSource audioSource;
    //Glove game objects used to deterime speed of gloves 
    private Transform rightGlove, leftGlove, tempGlove;
    //The values that hold the last and current values of speed for both gloves
    Vector3 lastPositionLeftGlove, lastPositionRightGlove;
    float speedLeftGlove, speedRightGlove;
    //The pitch the sound is set to every frame
    float pitch;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        FindGloves();
    }

    private void Update()
    {
        if (rightGlove != null)
        {
            //Takes current postion and subtracts from last position and divides it by time to find speed of glove
            speedRightGlove = (rightGlove.transform.position - lastPositionRightGlove).magnitude / Time.deltaTime;
            lastPositionRightGlove = rightGlove.transform.position;
        }
        else
        {
            Debug.Log("Could not find right glove.");
        }

        if (leftGlove != null)
        {
            //Takes current postion and subtracts from last position and divides it by time to find speed of glove
            speedLeftGlove = (leftGlove.transform.position - lastPositionLeftGlove).magnitude / Time.deltaTime;
            lastPositionLeftGlove = leftGlove.transform.position;
        }
        else
        {
            Debug.Log("Could not find left glove.");
        }
        //Grabs speed of both gloves and adds there normlized numbrers together
        pitch = Normalize(speedRightGlove);
        pitch += Normalize(speedLeftGlove);
        //Sets pitch to 2 if higher then 2 to not make pitch not greater then 3
        if (pitch > 2)
        {
            pitch = 2;
        }
        //Adds 1 to pitch to make the lowest pitch 1
        pitch++;
    }

    public void PlayOnHitSound()
    {
        //Sets the pitch of the sound based of punch speed
        audioSource.pitch = pitch;
        //Plays sound once
        audioSource.PlayOneShot(hitSound);
    }

    public void PlayOnHitSound(float pitchOverRide) // Sets Pitch based on float entered. Value between 0-3.
    {
        //Sets the pitch based on inserted value
        audioSource.pitch = pitchOverRide;
        //Plays sound once
        audioSource.PlayOneShot(hitSound);
    }

    public void PlayKnockDownSound()
    {
        //Sets the pitch of the sound to one
        audioSource.pitch = 1;
        //Plays sound once
        audioSource.PlayOneShot(KnockDownSound);
    }

    public void PlayThrowPunchSound()
    {
        //Sets the pitch of the sound to one
        audioSource.pitch = 1;
        //Plays sound once
        audioSource.PlayOneShot(ThrowPunchSound);
    }

    private void FindGloves()
    {
        //Goes through all the children of the boxers to find both gloves and sets them
        tempGlove = transform.Find("mixamorig:Hips");
        tempGlove = tempGlove.transform.Find("mixamorig:Spine");
        tempGlove = tempGlove.transform.Find("mixamorig:Spine1");
        tempGlove = tempGlove.transform.Find("mixamorig:Spine2");
        rightGlove = tempGlove.transform.Find("mixamorig:RightShoulder");
        rightGlove = rightGlove.transform.Find("mixamorig:RightArm");
        rightGlove = rightGlove.transform.Find("mixamorig:RightForeArm");
        rightGlove = rightGlove.transform.Find("mixamorig:RightHand");
        rightGlove = rightGlove.transform.Find("GloveR");
        leftGlove = tempGlove.transform.Find("mixamorig:LeftShoulder");
        leftGlove = leftGlove.transform.Find("mixamorig:LeftArm");
        leftGlove = leftGlove.transform.Find("mixamorig:LeftForeArm");
        leftGlove = leftGlove.transform.Find("mixamorig:LeftHand");
        leftGlove = leftGlove.transform.Find("GloveL");
    }

    private float Normalize(float speed)
    {
        //If speed is outside of 0-100 it is set to the nearest value.
        if (speed < 0)
        {
            speed = 0;
        }
        if (speed > 100)
        {
            speed = 100;
        }
        
        //Sets speed to be between 0 - 2
        speed = speed / 50;

        return speed;
    }
}
