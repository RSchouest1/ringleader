﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Perform a combo that involves the boxer switching between right and left hooks. 
//                 Increase the kop damage and increase stamina drain on block.
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: H, G, T
//        Dates 07/16/2017
//*/

//Author: Brandon Vitayanuvatti
//Date: 11/09/2017
//Purpose:Setting up code for Referee to register the illegal hit and makeing it possible to deal illegal hits since
// the only code prior is the debug under DoComboResult.
//Note: To help clarify who wrote what comments, I will put my name in front of my comments.
//Also, the keyboard command is C + 4.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Illegal_Procedure: Combo1
{
    //Brandon: The way I invision it is that the variable turns true when the boxer of corresponding color throws an illegal hit.
    bool hitterRed = false;
    bool hitterBlue = false;

    bool illegalHit = false;
    //Brandon: These three variables keep track of how many times each Boxer has landed an Illegal Punch.
    //I also prepared a total count for Illegal punches which will likely be used for analytics. I currently have no purpose for the third variable.
    int illegalCountRed;
    int illegalCountBlue;
    int illegalCountTotal;

    //Brandon: I need to reference the two boxers to determine whether to increment illegalCountRed or illegalCountBlue
    private GameObject redBox;//Look at statmanager.
    private GameObject blueBox;

    private GameObject referee;

    //This grabs the Command Manager from the Timer.
    private CommandManager cM;
    private HitManager hM;

    private BoxerStateMachine1 bsmRed; //BoxerStateMachine on the Red Boxer;
    private BoxerStateMachine1 bsmBlue;

    void Start()
    {
        illegalCountRed = 0;
        illegalCountBlue = 0;
        illegalCountTotal = 0;

        redBox = GameObject.FindGameObjectWithTag("RedBoxer");
        blueBox = GameObject.FindGameObjectWithTag("BlueBoxer");

        referee = GameObject.FindGameObjectWithTag("Referee");

        cM = GameObject.FindGameObjectWithTag("Timer").GetComponent<CommandManager>(); // Brandon: The Command Manager is on the Timer game object.

        bsmRed = redBox.GetComponent<BoxerStateMachine1>();
        bsmBlue = blueBox.GetComponent<BoxerStateMachine1>();
    }


    void Update()
    {

    }

    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        //Brandon: When a Referee throws an Illegal Call, he should first seperate the boxers, 
        //than deduct points from the boxer that threw the Illegal Hit.
        //So the seperate command should play out including animation, then immeadiately proceed to the AddPoint script
        //and animation. I don't think a new animation is needed for the Illegal Procedure when the AddPoint animation (PointRight) fits.
        Debug.Log("Illegal Procedure Success");

        if (illegalHit)
        {
            Debug.Log("Referee has called an Illegal Procedure. Now attempting to separate the boxers.");
            cM.Command_Separate();
            if (hitterRed)
            {
                Debug.Log("Red has been called out for making an Illegal Hit!");
                referee.GetComponent<AddPointPlayer1>().DoComboResult();
                illegalCountRed++; //Brandon: Keep track of how many illegal hits so it can be displayed during Focus Mode.
                illegalCountTotal++; //Brandon: I believe this is mostly needed for analytics and should not have much impact on the overall play session.

                bsmRed.dirtyRed = false; //Reset this otherwise it will always be red even if blue threw the hit.
                hitterRed = false; //Brandon: Resetting the variable at the end of the method.

            }
            else if (hitterBlue)
            {
                Debug.Log("Blue has been called out for making an Illegal Hit!");

                referee.GetComponent<AddPointPlayer2>().DoComboResult();
                illegalCountBlue++;
                illegalCountTotal++;

                bsmBlue.dirtyBlue = false;
                hitterBlue = false;
            }
        }

        
    }

    public override void Initilialize()
    {
   
    }

    public int getRedIllegal()
    {
        return illegalCountRed;
    }

    public int getBlueIllegal()
    {
        return illegalCountBlue;
    }

    //These methods will be called in the corresponding boxer's script that determines if their doing an illegal hit.
     
    public void illegalHitRed()
    { 
            hitterRed = true;
    }

    public void illegalHitBlue()
    {

            hitterBlue = true;

    }

    public void illegalHitThrown()
    {
        illegalHit = true;
    }
}