﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Nicholas O'Keefe
// 7/13/2017
// Now uses Input Axes and as a result all GetKey's have been replaced with GetButton's
// This will only work with the up to date Input Manager
// To check this goto Edit->ProjectSettings->Input, where as of (07/28/17) there should be a total of 50 Input Axes
//
// Supports Keyboard and Mouse, and Xbox 360 controller 
// If you want to use a PS4 controller see the "Differences between Xbox 360 and PS4 controllers in the Input Manager" trello card
//
// Calls ComboManager.HandleInput(string) after every punch input
*/

/* Daniel Marino Coyne
 * Last Updated: 08-16-17
 * 
 * Notes:
 * I worked on the Stamina system that Joshua Claassen created. I made it so that when the player does a left or right hook then
 * it will tick down the stamina value until it reaches zero, and when it reaches zero the player cannot punch anymore until the
 * stamina charges up again. I also did some minor organizational changes to this code to make it easier to navigate.
 */

// 8/13/2017
// Cheng-An Lin
// Rewrite the input function to send massage to statemachine.
// Check files in Assets -> Script -> Boxer_StateMachine to see what's up during each state
// Now we can add different effects or conditions in different punches without affecting the MovePunch1 state

//Aurthor:Kevin Ayad
//Date: 10/13/2017
//Credit: Kevin Ayad
//Purpose: Corner Mechanics

//Author:T. Miles Fowler
//Date: 10/18/2017
//Credit: T. Miles Fowler
//Purpose: Orthdox and Southpaw movement toggle

//Author:Hector Castelli Zacharias
//Date: 11/01/2017
//Credit: Hector Castelli Zacharias
//Purpose: Driving of the legs animations when input is detected

public class BoxerState_MovePunch1 : ByTheTale.StateMachine.State, IMovement, IPunching
{
    public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }

	private BoxerData1 bD1 = Component.FindObjectOfType<BoxerData1> ();
	 
	public Game_Timer gameTimer;


    Transform boxerTransform;
    string horizontalAxis;
    string verticalAxis;
    CharacterController characterController;
    GameObject enemyObject;
    ComboManager1 comboManager;


    public float curPlayerStamina;

    //DMC_Branch
    Vector3 moveDirection = Vector3.zero;
    int punchCode = 0;

    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        horizontalAxis = bSM.boxerData.horizontalAxis;
        verticalAxis = bSM.boxerData.verticalAxis;
        characterController = bSM.characterController;
        enemyObject = bSM.boxerData.enemyObj;
        comboManager = bSM.boxerData.comboManager;
    }
	public override void Enter()
    {
        base.Enter();
        bSM.ClearPunchCode();

		//Reference to the game timer in the ringleader scene
		gameTimer = Component.FindObjectOfType<Game_Timer> ();

    }
    public override void Execute()
    {
		
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);

		//Disbales and Enables movemnt using the Game timer 
		if(gameTimer.movement == false){

			return;
		} 
		else {

			HandleMovement();
			HandlePunching();
		}

		//HandleMovement();
		//HandlePunching();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public void HandleMovement()
    {
        if (Input.GetButton(bSM.boxerData.button_Crouch))
        {
            bSM.boxerAnimator.SetBool("isCrouching", true);
        }
        else
        {
            bSM.boxerAnimator.SetBool("isCrouching", false);
        }
        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
		//The line below toggles between Orthodox and Southpaw movement. True = Southpaw.
		if (bD1.isSouthpaw) {
			moveDirection = boxerTransform.TransformDirection (moveDirection);
		}
		moveDirection *= bSM.boxerData.moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        boxerTransform.position = new Vector3(boxerTransform.position.x, (Mathf.Abs(characterController.velocity.magnitude)>0)? 0: 0.1f, boxerTransform.position.z); //Keeps the boxer's feet on the boxing canvas.
        if (Input.GetButton(bSM.boxerData.verticalAxis))
        {
            bSM.staminaScript.MovementLoss();
        }
        if (Input.GetButton(bSM.boxerData.horizontalAxis))
        {
            bSM.staminaScript.MovementLoss();
        }

        MovementAnimation(characterController.velocity); //Trigger Animation SM with the velocity of the character controller
    }

    //Triggers animations on the current boxer depending on the movement
    //Author:Hector Castelli Zacharias
    //Date: 11/01/2017
    //Credit: Hector Castelli Zacharias
    //Purpose: Driving of the legs animations when input is detected
    public void MovementAnimation(Vector3 _moveDirection)
    {
        bSM.boxerAnimator.SetFloat("Speed", _moveDirection.magnitude / bSM.boxerData.moveSpeed);
        _moveDirection.Normalize();
        bSM.boxerAnimator.SetFloat("MovementX", _moveDirection.x);
        bSM.boxerAnimator.SetFloat("MovementZ", _moveDirection.z);

        //Debug code for value inspection.
        /*
        if (Mathf.Abs(_moveDirection.normalized.magnitude) >= 0.1f)
            Debug.LogError("Animator: x" + _moveDirection.x + " z:" + _moveDirection.z + ". speed " + bSM.boxerAnimator.GetFloat("Speed"));
        */
    }
    public void HandlePunching()
    {

        if (Input.GetButton(bSM.boxerData.button_Hook))
        {
            WhichHook();
        }
        else if (Input.GetButton(bSM.boxerData.button_Uppercut))
        {
            WhichUppercut();
        }
        else if (Input.GetButtonDown(bSM.boxerData.button_Left))
        {
            if (bSM.staminaScript.GetCurStamina() < 2)
            {
                // This makes it so that if the player does not have any stamina then they can't punch
                // until they reach full stamina again.
                Debug.Log("I can't jab");
            }
            else bSM.Jab();
        }
        else if (Input.GetButtonDown(bSM.boxerData.button_Right))
        {
            if (bSM.staminaScript.GetCurStamina() < 2)
            {
                // This makes it so that if the player does not have any stamina then they can't punch
                // until they reach full stamina again.
                Debug.Log("I can't cross");
            }
            else bSM.Cross();
        }
    }
    public void WhichHook()
    {
        if (Input.GetButtonDown(bSM.boxerData.button_Left))
            bSM.LeftHook();
        else if (Input.GetButtonDown(bSM.boxerData.button_Right))
            bSM.RightHook();
        else { bSM.LeftFakePunch(); }
    }
    public void WhichUppercut()
    {
        if (Input.GetButtonDown(bSM.boxerData.button_Left))
            bSM.LeftUppercut();
        else if (Input.GetButtonDown(bSM.boxerData.button_Right))
            bSM.RightUppercut();
        else { bSM.RightFakePunch(); }
    }

}

