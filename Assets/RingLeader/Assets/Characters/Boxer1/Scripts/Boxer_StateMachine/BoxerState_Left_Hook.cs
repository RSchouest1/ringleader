﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 10/18/17
// Jordan Ferris
// Now tells the glove script that a hook was thrown and increments hooks thrown in StatsManager

// Daniel Vanallen
// Contribution: Added a call to function "EnemyPunchCode" in order to send in the punch code and change the currentPC for reaction animations.
//    Feature: Blocking System & Hit Reaction Animation system
//    10/23/2017 - 11/19/2017
//    References: None
//    Links: Refer "void HitReaction"

public class BoxerState_Left_Hook : BoxerState_Basic {

	private Glove gloveScript;		// gets reference to Glove script

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();

		gloveScript = Component.FindObjectOfType<Glove> ();

		if (bSM.boxerAnimator.GetBool ("isCrouching")) {
			punchCode = 641;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this puncCode
		} else {
			punchCode = 41;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this punchCode
		}
			bSM.staminaScript.UseHook ();
			comboManager.HandleInput (bSM.boxerData.button_Left);
			comboManager.HandleInput (bSM.boxerData.button_Hook);
			bSM.boxerAnimator.SetInteger ("PunchCode", punchCode);
		

		bSM.statsScript.HookThrown ();		// increments hooks thrown in StatsManager script
		gloveScript.SetPunch ("hook");		// tells the glove script that a hook was thrown
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit(); 
    }
}
