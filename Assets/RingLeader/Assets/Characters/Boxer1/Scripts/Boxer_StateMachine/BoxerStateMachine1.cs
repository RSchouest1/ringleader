﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 7/13/2017
// Nicholas O'Keefe
// This file was only changed to support duplicated scripts
// and should be reverted to the original after testing
//
// 8/23/2017
// Cheng-An Lin
// Now each punchese has it's own state.
//
//10/18/2017
//Azier Larranaga
//Added the switch statement to call the Freestyle Combo System (press Y)
//All declarations and functions are commented

//10/18/2017
//Cooper Schneider
//Added bools and accessors and mutators for them that help tell when a state has been activated for TrainStateManager

//10/18/2017
//Jordan Ferris
//Now has a reference to StatsManager

// Daniel Vanallen
// Contribution: Added Blocking Command System and isCrouching declaration variable and added function manipulation for currentPC changes
//    Feature: Blocking System & Data Accessors for Hit Reactions & Illegal Hit system
//    10/23/2017 - 11/19/2017
//    References: None
//    Links: Refer to region "Block Command Setup" of this script and EnemyPunchCode() function at the bottom of this script.
// 
// Brandon Vitayanuvatti
// Date: 11/13/2017
// Addition: Adding two variables to determine which Boxer was the one that threw an Illegal Hit. I need to determine this in order for the Referee's Illegal Procedure script to work.


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxerData1))]
[RequireComponent(typeof(CharacterController))]

public class BoxerStateMachine1 : ByTheTale.StateMachine.MachineBehaviour
{
    [HideInInspector] public Animator boxerAnimator;
    [HideInInspector] public Animator ringAnimator;
    [HideInInspector] public BoxerData1 boxerData;
    [HideInInspector] public CharacterController characterController;

    [HideInInspector] public ANY_KOP KopSystem;

    // Allowing the Boxers to know about the GameStateManager
    [HideInInspector] public GameStateManager gSM;

    [HideInInspector] public Stamina staminaScript;
    // ^Added by Daniel Coyne

	[HideInInspector] public StatsManager statsScript;

    //MZ_RingLeader
    public GameObject Ring;

	//Bools to determine if the state has been called
	private bool didJab, didLeftHook, didRightHook, didLeftUppercut, didRightUppercut, didLeftFake, didRightFake;

	//added by Azier Larranaga
	//List for the punches to be saved
	public List<string> comboHeapInputs = new List<string> ();
	//Timer for the combo heap
	private float comboHeapTimer = 0f;
	//combo Heap mode (freestyle)
	public bool comboFree = false;
	//enum for states for the freestyle
	public enum FreeStyle 
	{
		Idle,
		Recording,
		Release,
		Recovery
	}
	//Sets current state to idle
	FreeStyle curState = FreeStyle.Idle;
	//punchcode for calling the punches
	protected int punchCode = 0;
	//currentPC that doesn't get changed until new punch for hit reaction animations.
	public int currentPC = 0;
	//animator call to control the attacks frequency
	public int animAttackCall = 0;
	//int that controls the distributed for statement
	public int nextAttackControl = -1;
    //end Azier Larranaga declarations

	public bool dirtyPunchMode = false;
	public bool dirtyPunchSetup = false;

    public bool dirtyRed = false; //Set true if red did the Illegal Hit
    public bool dirtyBlue = false;


	//To let target know attacker is crouching in HitManager.cs
	public bool isCrouching = false;
    

    public override void AddStates()
    {
        AddState<BoxerState_MovePunch1>();
        AddState<BoxerState_Blocking1>();
        AddState<BoxerState_Commanded>();
        AddState<BoxerState_Dodging>();
        AddState<BoxerState_Hugging>();
        AddState<BoxerState_KnockedDown>();
        AddState<BoxerState_Paused>();
        AddState<BoxerState_Jab>();
        AddState<BoxerState_Cross>();
        AddState<BoxerState_Left_Hook>();
        AddState<BoxerState_Right_Hook>();
        AddState<BoxerState_Left_Uppercut>();
        AddState<BoxerState_Right_Uppercut>();
        AddState<BoxerState_Left_FakePunch>();
        AddState<BoxerState_Right_FakePunch>();

        SetInitialState<BoxerState_Paused>();
    }

    public void Awake()
    {
        // Initializing the Game State Manager
        gSM = GameObject.FindObjectOfType<GameStateManager>();
        //////////////////////////////////////////////////////

        boxerAnimator = GetComponent<Animator>();
        ringAnimator = Ring.GetComponent<Animator>();
        boxerData = GetComponent<BoxerData1>();
        characterController = GetComponent<CharacterController>();
        staminaScript = GetComponent<Stamina>();
		statsScript = GetComponent<StatsManager> ();

    }
    public override void LateUpdate()
    {
        base.LateUpdate();
    }
    public void KnockDown()
    {

        if (ringAnimator.GetBool("RopesMove") == false);
        {
            ringAnimator.SetBool("RopesMove", true);
            boxerAnimator.SetTrigger("isKD");
            ChangeState<BoxerState_KnockedDown>();
        }
    }

	//Azier Larranaga ----- This update is added for the combo heap to work properly. It allows ease of access to all of the functions for the list.
	public override void Update() //added for the combo heap
	{
		base.Update ();

		#region Block Command Setup
		isCrouching = boxerAnimator.GetBool("isCrouching");
		if(Input.GetButton (boxerData.button_Defend))
		{
			if (isCrouching)
			{
				Glove_Block_Disable ();
				Glove_Block_Enable_Body ();
				boxerAnimator.SetInteger ("PunchCode", punchCode);
			} 
			else 
			{
				Glove_Block_Disable ();
				Glove_Block_Enable_Head ();
				boxerAnimator.SetInteger ("PunchCode", punchCode);
			}
		}
		else if(Input.GetButtonUp (boxerData.button_Defend))
		{
			Glove_Block_Disable ();
			boxerAnimator.SetInteger ("PunchCode", punchCode);
		}
		#endregion
		//This system sets up the dirty punch mode depending on if the player is holding
		//down crouch and dodge button at the same time.
		#region Illegal Punch Command (dirtyPunching)
		if (Input.GetButtonDown (boxerData.button_Dodge)) 
		{
			dirtyPunchSetup = true;
		}
		else if(Input.GetButtonUp (boxerData.button_Dodge))
		{
			dirtyPunchSetup = false;
		}

		if (isCrouching && dirtyPunchSetup) 
		{
			dirtyPunchMode = true;
		}
		else if(!isCrouching || !dirtyPunchSetup)
		{
			dirtyPunchMode = false;
		}
		#endregion

		switch(curState)
		{
		case FreeStyle.Idle://waiting for combo heap to be activated
			//Debug.Log ("In Idle");
			if (Input.GetKeyDown (KeyCode.Y))
			{
				comboHeapTimer += Time.deltaTime;//timer for th eplayer to introduce the inputs
				curState = FreeStyle.Recording;
				StopAttacksForCH ();//stops the Script from turning the attacks
			}
			break;
		case FreeStyle.Recording://Player Saves the inputs
			//Debug.Log ("Entered Recording");
			comboFree = true;
			if(Input.GetKeyDown (KeyCode.R) && comboFree)//In the instance of hitting "F", addit to the list. Works same way for subsequent parts of the code
			{
				comboHeapInputs.Add ("R");//LEFT JAB
			}
			else if(Input.GetKeyDown (KeyCode.T) && comboFree)//like this one.
			{
				comboHeapInputs.Add ("T");//RIGHT JAB
			}
			else if(Input.GetKeyDown (KeyCode.H) && comboFree)
			{
				comboHeapInputs.Add ("H");//CROUCH
			}
			else if(Input.GetKeyDown (KeyCode.F) && comboFree)
			{
				comboHeapInputs.Add ("F");//LEFT FAKE PUNCH
			}
			else if(Input.GetKeyDown (KeyCode.G) && comboFree)
			{
				comboHeapInputs.Add ("G");//RIGHT FAKE PUNCH
			}
			else if(Input.GetKeyDown (KeyCode.F) && Input.GetKeyDown (KeyCode.R) && comboFree)
			{
				comboHeapInputs.Add ("FR");//LEFT HOOK
			}
			else if(Input.GetKeyDown (KeyCode.F) && Input.GetKeyDown (KeyCode.T) && comboFree)
			{
				comboHeapInputs.Add ("FT");//RIGHT HOOK
			}
			else if(Input.GetKeyDown (KeyCode.G) && Input.GetKeyDown (KeyCode.R) && comboFree)
			{
				comboHeapInputs.Add ("GR");//LEFT UPPERCUT
			}
			else if(Input.GetKeyDown (KeyCode.G) && Input.GetKeyDown (KeyCode.T) && comboFree)
			{
				comboHeapInputs.Add ("GT");//RIGHT UPPERCUT
			}
			else if(Input.GetKeyDown (KeyCode.H) && Input.GetKeyDown (KeyCode.R) && comboFree)
			{
				comboHeapInputs.Add ("HR");//LEFT PUNCH WHILE CROUCH 
			}
			else if(Input.GetKeyDown (KeyCode.H) && Input.GetKeyDown (KeyCode.T) && comboFree)
			{
				comboHeapInputs.Add ("HT");//RIGHT PUNCH WHILE CROUCHING
			}
			else if(Input.GetKeyDown (KeyCode.H) && Input.GetKeyDown (KeyCode.G) && Input.GetKeyDown (KeyCode.R) && comboFree)
			{
				comboHeapInputs.Add ("HGR");//LEFT UPPERCUT WHILE CROUCH
			}
			else if(Input.GetKeyDown (KeyCode.H) && Input.GetKeyDown (KeyCode.G) && Input.GetKeyDown (KeyCode.T) && comboFree)
			{
				comboHeapInputs.Add ("HGT");//RIGHT UPPERCUT WHILE CROUCHING
			}
			if(Input.GetKeyDown (KeyCode.Y) && comboFree || comboHeapTimer >= 5f)//releasing the combo
			{
				//for each loop to allow the combo heap to be displayed.
				comboFree = false;//assures recording is off
				animAttackCall = 1;//begins the calls for release
				nextAttackControl = 0;//resets the index to 0
				StartAttackForCH ();//stops the character from attacking while recording
				curState = FreeStyle.Release;//changes state to release the recorded punches
			}
			break;
		case FreeStyle.Release://player throws the ounches on this state that he inputed
			if(animAttackCall == 1)
			{
				if (nextAttackControl < comboHeapInputs.Count) //as long as the attack control is not higher than the count, the player will continue to do the recorded moves.
				{
					Debug.Log ("Index:" + nextAttackControl+". Count:" + comboHeapInputs.Count.ToString ());
					if (comboHeapInputs [nextAttackControl] == "R") //Left Jab
					{
						if (isCrouching) {
							punchCode = 61;
						} else {
							punchCode = 1;
						}
						staminaScript.LeftJab ();
						boxerData.comboManager.HandleInput (boxerData.button_Left);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						
					} 
					else if (comboHeapInputs [nextAttackControl] == "T") //Right Jab
					{
						if (isCrouching) {
							punchCode = 62;
						} else {
							punchCode = 2;
						}
						staminaScript.RightJab ();
						boxerData.comboManager.HandleInput (boxerData.button_Right);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						
					}
					else if (comboHeapInputs [nextAttackControl] == "H") 
					{
							Debug.Log ("Crouching");
					} 
					else if (comboHeapInputs [nextAttackControl] == "F") 
					{
						punchCode = 21;
						boxerData.comboManager.HandleInput (boxerData.button_Left);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
					} 
					else if (comboHeapInputs [nextAttackControl] == "G") 
					{
						punchCode = 22;
						boxerData.comboManager.HandleInput (boxerData.button_Left);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
					}
					else if (comboHeapInputs [nextAttackControl] == "FR")
					{
						if (isCrouching) {
							punchCode = 641;
						} else {
							punchCode = 41;
						}
						staminaScript.UseHook ();
						boxerData.comboManager.HandleInput (boxerData.button_Left);
						boxerData.comboManager.HandleInput (boxerData.button_Hook);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						
					}
					else if (comboHeapInputs [nextAttackControl] == "FT") 
					{
						if (isCrouching) {
							punchCode = 642;
						} else {
							punchCode = 42;
						}
						staminaScript.UseHook ();
						boxerData.comboManager.HandleInput (boxerData.button_Right);
						boxerData.comboManager.HandleInput (boxerData.button_Hook);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						
					}
					else if (comboHeapInputs [nextAttackControl] == "GR") 
					{
						if (isCrouching) {
							punchCode = 651;
						} else {
							punchCode = 51;
						}
						staminaScript.UseUppercut ();
						boxerData.comboManager.HandleInput (boxerData.button_Left);
						boxerData.comboManager.HandleInput (boxerData.button_Uppercut);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						

					}
					else if (comboHeapInputs [nextAttackControl] == "GT") 
					{
						if (isCrouching) {
							punchCode = 652;
						} else {
							punchCode = 52;
						}
						staminaScript.UseUppercut ();
						boxerData.comboManager.HandleInput (boxerData.button_Right);
						boxerData.comboManager.HandleInput (boxerData.button_Uppercut);
						boxerAnimator.SetInteger ("PunchCode", punchCode);
						
					}
					else if (comboHeapInputs [nextAttackControl] == "HR") 
					{
							Debug.Log ("Jab crouch");
					
					}
					else if (comboHeapInputs [nextAttackControl] == "HT") 
					{
							Debug.Log ("jab crouch");
					
					}
					else if (comboHeapInputs [nextAttackControl] == "HGR")
					{
							Debug.Log ("uppercut crouch left");
					
					}
					else if (comboHeapInputs [nextAttackControl] == "HGT") 
					{
							Debug.Log ("uppercut crouch right");
					}
				}
			}
			if (animAttackCall == 2)
			{
				nextAttackControl++;
				animAttackCall = 1;
			} 
			if (nextAttackControl > comboHeapInputs.Count || animAttackCall == 0) 
			{
				punchCode = 0;
				comboHeapTimer = 0F;
				curState = FreeStyle.Recovery;
				nextAttackControl = 0;
			}
			break;
		case FreeStyle.Recovery://Combo Heap is off until this is clear
			//Here the stamina will be reduced from the boxer.
			Debug.Log ("Entered recovery");
			comboHeapInputs.Clear ();
			if(staminaScript.hasStamina== false)
			{
				comboHeapTimer += Time.deltaTime;
				if (comboHeapTimer == 5.0f)
					curState = FreeStyle.Idle;
			}
			else if(staminaScript.hasStamina == true)
			{
				curState = FreeStyle.Idle;
			}	
			break;
		default:
			Debug.Log ("Reached default");
			break;
		}
	}


    // Santiago Castaneda / Ring Leader August Version: Adding an Stand Up function 
    // to allow the player to Stand up after being Knock Down
    public void StandUp()
    {
        // This space is to activate the animation to Stand up
        // Right now it uses a simple trigger (it can be changed at any time).
        boxerAnimator.SetTrigger("isUP");
        //

        // Changing the Boxer State to the proper State to Fight

        // This function needs improvement.

        AllowBoxerToFightAfterStandingCountState(gSM.BoxerInput);

        AllowBoxerToFightAfterStandingCountState(gSM.BoxerInput_2);

        gSM.ChangeGameState(Constants.GameState.RoundAction);
    }

    void AllowBoxerToFightAfterStandingCountState(GameObject BoxerGameObject) {

        BoxerGameObject.GetComponent<BoxerStateMachine1>().ChangeState<BoxerState_MovePunch1>();

        BoxerGameObject.GetComponent<ANY_KOP>().IsKD = true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //4 YikunGuo
    public void Blocking()
    {
        ChangeState<BoxerState_Blocking1>();
    }
    public void MovePunch()
    {
        ChangeState<BoxerState_MovePunch1>();
    }
    public void Dodge()
    {
        ChangeState<BoxerState_Dodging>();
    }
    public void Jab()
    {
        ChangeState<BoxerState_Jab>();
		didJab = true;
    }
    public void Cross()
    {
        ChangeState<BoxerState_Cross>();
    }
    public void LeftHook()
    {
        ChangeState<BoxerState_Left_Hook>();
		didLeftHook = true;
    }
    public void RightHook()
    {
        ChangeState<BoxerState_Right_Hook>();
		didRightHook = true;
    }
    public void LeftUppercut()
    {
        ChangeState<BoxerState_Left_Uppercut>();
		didLeftUppercut = true;
    }
    public void RightUppercut()
    {
        ChangeState<BoxerState_Right_Uppercut>();
		didRightUppercut = true;
    }
    public void LeftFakePunch()
    {
        ChangeState<BoxerState_Left_FakePunch>();
		didLeftFake = true;
    }
    public void RightFakePunch()
    {
        ChangeState<BoxerState_Right_FakePunch>();
		didRightFake = true;
    }


    #region Animation Event Methods
    public void ToggleIsPunching(int i)
    {
        if (i == 0) {
            boxerData.isPunching = false;
        } else {
            boxerData.isPunching = true;
        }
    }

    public void RightGlove_Damage_Enable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.RightGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }
    public void RightGlove_Damage_Disable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.RightGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }

    public void LeftGlove_Damage_Enable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.LeftGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }
    public void LeftGlove_Damage_Disable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.LeftGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }

    public void Glove_Block_Enable_Head()
    {
        boxerData.isBlockEnabled = true;
		punchCode = 3;
    }
    public void Glove_Block_Disable()
    {
		boxerData.isBodyBlockEnabled = false;
        boxerData.isBlockEnabled = false;
		punchCode = 0;
    }
	public void Glove_Block_Enable_Body()
	{
		boxerData.isBodyBlockEnabled = true;
		punchCode = 63;
	}

    public void ClearPunchCode()
    {
        boxerAnimator.SetInteger("PunchCode", 0);
    }
    #endregion

	//Azier Larranaga --- Functions for the freestyle
	//This function will eliminate the function that causes the player to attack.
	void StopAttacksForCH ()
	{
		Debug.Log ("script is off");
		boxerAnimator.enabled = false;

	}
	//restarts the cript that allows the characters to punch. AL
	void StartAttackForCH()
	{
		boxerAnimator.enabled = true;

	}
	//Calls the stops for the attacks to happen during release
	void NextAttack()
	{
		animAttackCall = 2;
	}
	//end Azier Larranaga

	//Getters and Setters for each of the bools that determine if the state went off

	public bool getDidJab()
	{
		return didJab;
	}

	public void toggleDidJab()
	{
		didJab = !didJab;
	}

	public bool getDidLeftHook()
	{
		return didLeftHook;
	}

	public void toggleDidLeftHook()
	{
		didLeftHook = !didLeftHook;
	}

	public bool getDidRightHook()
	{
		return didRightHook;
	}

	public void toggleDidRightHook()
	{
		didRightHook = !didRightHook;
	}

	public bool getDidLeftUppercut()
	{
		return didLeftUppercut;
	}

	public void toggleDidLeftUppercut()
	{
		didLeftUppercut = !didLeftUppercut;
	}

	public bool getDidRightUppercut()
	{
		return didRightUppercut;
	}

	public void toggleDidRightUppercut()
	{
		didRightUppercut = !didRightUppercut;
	}

	public bool getDidLeftFake()
	{
		return didLeftFake;
	}

	public void toggleDidLeftFake()
	{
		didLeftFake = !didLeftFake;
	}

	public bool getDidRightFake()
	{
		return didRightFake;
	}

	public void toggleDidRightFake()
	{
		didRightFake = !didRightFake;
	}

	//this is to determine which hit was made in the HitManager so system can react depending on hit.
	public int EnemyPunchCode(int swingCode)
	{
		//The input from each hit will change the currentPC to the punchCode for the hit and return the new currentPC
		currentPC = swingCode;
		return currentPC;
	}
}