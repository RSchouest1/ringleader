﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 7/19/2017
// Nicholas O'Keefe
// Now uses Input Axes and as a result all GetKey's have been replaced with GetButton's
// This will only work with the up to date Input Manager
// To check this goto Edit->ProjectSettings->Input, where as of (07/21/17) there should be a total of 48 Input Axes
//
// Supports Keyboard and Mouse, and Xbox 360 controller though it still needs polish 
//


//Author:Hector Castelli Zacharias
//Date: 11/01/2017
//Credit: Hector Castelli Zacharias
//Purpose: Driving of the legs animations when input is detected

public class BoxerState_Blocking1 : ByTheTale.StateMachine.State, IMovement
{
    public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }

    Transform boxerTransform;
    string horizontalAxis;
    string verticalAxis;
    CharacterController characterController;
    GameObject enemyObject;

    Vector3 moveDirection = Vector3.zero;

    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        horizontalAxis = bSM.boxerData.horizontalAxis;
        verticalAxis = bSM.boxerData.verticalAxis;
        characterController = bSM.characterController;
        enemyObject = bSM.boxerData.enemyObj;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);

        HandleMovement();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void HandleMovement()
    {   
        if (Input.GetButton(bSM.boxerData.button_Crouch))
        {
            bSM.boxerAnimator.SetBool("isCrouching", true); 
        }
        else
        {
            bSM.boxerAnimator.SetBool("isCrouching", false);
        }
        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = boxerTransform.TransformDirection(moveDirection);
        moveDirection *= bSM.boxerData.moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        boxerTransform.position = new Vector3(boxerTransform.position.x, (Mathf.Abs(characterController.velocity.magnitude) > 0) ? 0 : 0.1f, boxerTransform.position.z); //Keeps the boxer's feet on the boxing canvas.

        MovementAnimation(characterController.velocity); //Trigger Animation SM with the velocity of the character controller
    }


    //Triggers animations on the current boxer depending on the movement
    //Author:Hector Castelli Zacharias
    //Date: 11/01/2017
    //Credit: Hector Castelli Zacharias
    //Purpose: Driving of the legs animations when input is detected
    public void MovementAnimation(Vector3 _moveDirection)
    {
        bSM.boxerAnimator.SetFloat("Speed", _moveDirection.magnitude / bSM.boxerData.moveSpeed);
        _moveDirection.Normalize();
        bSM.boxerAnimator.SetFloat("MovementX", _moveDirection.x);
        bSM.boxerAnimator.SetFloat("MovementZ", _moveDirection.z);

        //Debug code for value inspection.
        /*
        if (Mathf.Abs(_moveDirection.normalized.magnitude) >= 0.1f)
            Debug.LogError("Animator: x" + _moveDirection.x + " z:" + _moveDirection.z + ". speed " + bSM.boxerAnimator.GetFloat("Speed"));
        */
    }

    public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }

}