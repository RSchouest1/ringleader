﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 10/18/17
// Jordan Ferris
// Now increments knockdowns in StatsManager

//Author: Brandon Vitayanuvatti
//Date: 11/07/2017
//Purpose: Adding Standing Count animation. Need to add reference to Ref animator for checking if boxer gets up.

//Author: Luke Gillen
//Date: 11/13/17
//Purpose: Changing standup input to detect for controller buttons in the combination AXYB

public class BoxerState_KnockedDown : ByTheTale.StateMachine.State
{

    private Animator refAnim;

    public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }

	bool Key1Press = false;

	bool Key2Press = false;

	bool Key3Press = false;

	bool Key4Press = false;

	int TimesEntered = 0;

	int A1 = 0;

	int A2 = 0;

	int A3 = 0;

	int A4 = 0;

	public override void Initialize()
	{
		base.Initialize();
	}

	public override void Enter()
	{
		base.Enter();
        refAnim = GameObject.FindGameObjectWithTag("Referee").GetComponent<Animator>();
		CommandManager.instance.Command_KOStart(bSM);

		A1 = 0;

		A2 = 0;

		A3 = 0;

		A4 = 0;

		TimesEntered++;

		Key1Press = false;

		Key2Press = false;

		Key3Press = false;

		Key4Press = false;

		if (GetMachine<BoxerStateMachine1> ().gameObject.name == "Boxer_Red") {

			GameStateManager.gameStateManager.redBoxer++;
		}

		else{
			GameStateManager.gameStateManager.blueBoxer++;
		}

		Debug.Log (TimesEntered);
		bSM.statsScript.Knockdown ();		// increments knockdowns in StatsManager script
	}

	public override void Execute()
	{
		base.Execute();
        if (GetMachine<BoxerStateMachine1>().boxerData.boxerColor == BoxerData1.BoxerColor.Red)
        {
            if (Input.GetKeyDown(KeyCode.V) || Input.GetButton("Red_Uppercut"))
            {
                A1++;
                Debug.Log("V has been pressed.");
                if (A1 >= TimesEntered)
                {
                    Key1Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }

            }

            if (Input.GetKeyDown(KeyCode.B) || Input.GetButton("Red_Crouch"))
            {
                A2++;
                Debug.Log("B has been pressed.");
                if (A2 >= TimesEntered)
                {
                    Key2Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }

            if (Input.GetKeyDown(KeyCode.N) || Input.GetButton("Red_Defend"))
            {
                A3++;
                Debug.Log("N has been pressed.");
                if (A3 >= TimesEntered)
                {
                    Key3Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }

            if (Input.GetKeyDown(KeyCode.M) || Input.GetButton("Red_Hook"))
            {
                A4++;
                Debug.Log("M has been pressed.");
                if (A4 >= TimesEntered)
                {
                    Key4Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }
        } 
        else
        {
            if (Input.GetKeyDown(KeyCode.V) || Input.GetButton("Blue_Uppercut"))
            {
                A1++;
                Debug.Log("V has been pressed.");
                if (A1 >= TimesEntered)
                {
                    Key1Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }

            }

            if (Input.GetKeyDown(KeyCode.B) ||  Input.GetButton("Blue_Crouch"))
            {
                A2++;
                Debug.Log("B has been pressed.");
                if (A2 >= TimesEntered)
                {
                    Key2Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }

            if (Input.GetKeyDown(KeyCode.N) ||  Input.GetButton("Blue_Defend"))
            {
                A3++;
                Debug.Log("N has been pressed.");
                if (A3 >= TimesEntered)
                {
                    Key3Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }

            if (Input.GetKeyDown(KeyCode.M) || Input.GetButton("Blue_Hook"))
            {
                A4++;
                Debug.Log("M has been pressed.");
                if (A4 >= TimesEntered)
                {
                    Key4Press = true;
                    Debug.Log("<color=red>DONE</color>");
                }
            }
        }
		

		if (Key1Press == true && Key2Press == true && Key3Press == true && Key4Press == true)
		{
			Debug.Log("Standing up");
            refAnim.SetBool("BoxerDown", false);
			bSM.StandUp();         
		}
	}

	public override void Exit()
	{
		base.Exit();
	}
}