﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 10/18/17
// Jordan Ferris
// Now tells the glove script that an uppercut was thrown and increments uppercuts thrown in StatsManager

// Daniel Vanallen
// Contribution: Added a call to function "EnemyPunchCode" in order to send in the punch code and change the currentPC for reaction animations.
//    Feature: Blocking System & Hit Reaction Animation system
//    10/23/2017 - 11/19/2017
//    References: None
//    Links: Refer "void HitReaction"

public class BoxerState_Right_Uppercut : BoxerState_Basic {

	private Glove gloveScript;		// gets reference to Glove script

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();

		gloveScript = Component.FindObjectOfType<Glove> ();

		if (bSM.boxerAnimator.GetBool ("isCrouching")) {
			punchCode = 652;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this punchCode
		} else {
			punchCode = 52;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this punchCode
		}
			bSM.staminaScript.UseUppercut ();
			comboManager.HandleInput (bSM.boxerData.button_Right);
			comboManager.HandleInput (bSM.boxerData.button_Uppercut);
			bSM.boxerAnimator.SetInteger ("PunchCode", punchCode);
		
		bSM.statsScript.UppercutThrown ();		// increments uppercuts thrown in StatsManager script
		gloveScript.SetPunch ("uppercut");		// tells the glove script that an uppercut was thrown
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
		base.Exit();
    }
}
