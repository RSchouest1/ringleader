﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 10/18/17
// Jordan Ferris
// Now tells the glove script that a jab was thrown and increments jabs thrown in StatsManager

// Daniel Vanallen
// Contribution: Added a call to function "EnemyPunchCode" in order to send in the punch code and change the currentPC for reaction animations.
//    Feature: Blocking System & Hit Reaction Animation system
//    10/23/2017 - 11/19/2017
//    References: None
//    Links: Refer "void HitReaction"
public class BoxerState_Jab : BoxerState_Basic
{
	private Glove gloveScript;		// gets reference to Glove script

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();

		gloveScript = Component.FindObjectOfType<Glove> ();

		if (bSM.boxerAnimator.GetBool ("isCrouching")) {
			punchCode = 61;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this punchCode

		} else {
			punchCode = 1;
			bSM.EnemyPunchCode (punchCode);//to change currentPC to this punchCode
		}
			bSM.staminaScript.LeftJab ();
			comboManager.HandleInput (bSM.boxerData.button_Left);
			bSM.boxerAnimator.SetInteger ("PunchCode", punchCode);
		

        Debug.Log(bSM);
		bSM.statsScript.JabThrown ();		// increments jabs thrown in StatsManager script
		gloveScript.SetPunch ("jab");       // tells the glove script that a jab was thrown
    }

    public override void Execute()
    {
        base.Execute();

    }

    public override void Exit()
    {
        base.Exit();
    }
}
