﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Basic setup to separate boxers if they are not on the ropes or in the corner
//              This feature can be used by pressing E at any time.
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/
//
//Date: 11/04/2017
//Author: Brandon Vitayanuvatti
//Purpose: Adding Animation for Separate. Need to make accessor to call right animation.
//Note: To clarify which comments are by who, I will put my name in front of my comments.
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Separate : MonoBehaviour
{

    [SerializeField]
    CornerSeparate corner1, corner2, corner3, corner4;
    [SerializeField]
    RopeSeparate rope1, rope2, rope3, rope4;
    [SerializeField]
    string boxerToCheckFor, boxerToCheckFor2;
    GameObject boxer, boxer2;

    private bool canSeparate = false; //Brandon: Since SeparateBegin is declared withing DoComboResult, the accesor can't access it directly.

    // Use this for initialization
    void Start () {
        boxer = GameObject.Find(boxerToCheckFor);
        boxer2 = GameObject.Find(boxerToCheckFor2);

        
    }

    void OnEnable()
    {
        CommandManager.SeperateTheBoxers += MoveBoxers;
    }

    void OnDisable()
    {
        CommandManager.SeperateTheBoxers -= MoveBoxers;
    }
	
    void MoveBoxers()
    {

        float dist = Vector3.Distance(boxer.transform.position, boxer2.transform.position);
        if (dist < 3)
        {
            if (corner1.useThis == false && corner2.useThis == false && corner3.useThis == false && corner4.useThis == false)
            {
                if (rope1.useThis == false && rope2.useThis == false && rope3.useThis == false && rope4.useThis == false)
                {
                    canSeparate = true; //Brandon: I need to set canSeparate here since this is where it confirms the Boxer's separation.
                    boxer2.transform.position += (boxer2.transform.position - boxer.transform.position) / 2;
                    boxer.transform.position += (boxer.transform.position - boxer2.transform.position) / 2;
                }
            }
            else
            {
                canSeparate = false;
            }
        }
        else
            Debug.Log("The players are not close enough!");
    }


    public bool getCanSeparate()//Brandon: If this value is true, the separate animation will play. Referenced by RefereeState_Command script.
    {
        return canSeparate;
    }

    public void setCanSeparate(bool setTo) //Reset variable after successful separation inf RefereeState_Command script.
    {
        canSeparate = setTo;
    }
}
