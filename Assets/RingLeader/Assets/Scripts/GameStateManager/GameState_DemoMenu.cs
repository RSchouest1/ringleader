﻿/*------------------------------------------------------------------------------------------------
Original Author: Alex Ault
Date: 11/9/17
Credit:
Purpose: This state manages the Demo's altered MainMenu scene. Ref: https://trello.com/c/HZDN4aU4
Worked on by:
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_DemoMenu : GameState
{
    public override void InitializeState()
    {
        //Debug outputs and GameState status tracking
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));
        currentStatus = StateStatus.Staged;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        //Change to DemoMenu scene on Start
        RingLeaderSceneManager.instance.ChangeScene(9);
        
        //Debug outputs and GameState status tracking
        currentStatus = StateStatus.Start;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {

    }

    public override void ExitState()
    {
        //Debug outputs and GameState status tracking
        currentStatus = StateStatus.Exit;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }
}
