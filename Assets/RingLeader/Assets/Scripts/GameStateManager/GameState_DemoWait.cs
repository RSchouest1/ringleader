﻿/*------------------------------------------------------------------------------------------------
Original Author: Alex Ault
Date: 11/1/17
Credit:
Purpose: This state manages the Demo's waiting screen. Ref: https://trello.com/c/HZDN4aU4
Worked on by:
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_DemoWait : GameState
{
  //Variables

  //GameState overridden functions
    public override void InitializeState()
    {
        //Debug outputs and GameState status tracking
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));
        currentStatus = StateStatus.Staged;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        RingLeaderSceneManager.instance.ChangeScene(8);

        //Debug outputs and GameState status tracking
        currentStatus = StateStatus.Start;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {

    }

    public override void ExitState()
    {
        //Debug outputs and GameState status tracking
        currentStatus = StateStatus.Exit;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

  //Function to be used with a back button; not currently in use
    public void ReturnToMenu()
    {
        GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.Menus);
    }
}
