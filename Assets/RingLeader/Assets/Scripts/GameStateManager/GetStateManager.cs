﻿/*------------------------------------------------------------------------------------------------
Original Author: Unknown
Date: Unknown
Credit: 
Purpose: This script gets the global GameStateManager and calls its funtions based on UI button clicks.
Worked on by: Alex Ault
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetStateManager : MonoBehaviour
{
    public GameStateManager GSM;

	void Start ()
    {
        GSM = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}
	

    public void ChangeStateToBoxerSelect()
    {
        GSM.ChangeGameState(Constants.GameState.BoxerSelection);
    }

	public void ChangeStateToTrain ()
    {
		GSM.ChangeGameState (Constants.GameState.TrainingConditions);
	}

	public void ChangeStateToRef ()
    {
		GSM.ChangeGameState (Constants.GameState.TrainingRef);
	}

    public void ChangeStateToDemo ()
    {
        GSM.ChangeGameState(Constants.GameState.DemoWait);
    }
		
	public void SetToggleRedToTrue()
	{
		GSM.SetToggleRedToTrue ();
	}
	public void SetToggleRedToFalse()
	{
		GSM.SetToggleRedToFalse ();
    }
    public void SetToggleBlueToTrue()
    {
        GSM.SetToggleBlueToTrue();
    }
    public void SetToggleBlueToFalse()
    {
        GSM.SetToggleBlueToFalse();
    }
}
