﻿
/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
               Alex Ault - added in checks to see if demo mode is on or off to both return functions
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_GameOver : GameState
{

    //references/parameters/variables needed by this gamestate


    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;

        //TODO: anything this game state needs to do first when taking over as the current game state.

        /// Santiago Castaneda Munoz / Ring Leader August Version: 
        /// When enterint the Game Over State the game should transition to the Game Over Scene
        RingLeaderSceneManager.instance.ChangeScene(5);

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate

    public void ReturnToMenus()
    {
        /// Santiago Castaneda Munoz / Ring Leader August Version
        /// Adding the Functionality to go back to the main Menu

      //If Demo mode is off, return to main menu
        if (GameStateManager.gameStateManager.GetDemoMode() == GameStateManager.DemoOnOff.Off)
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.Menus);

      //If Demo mode is on, reutrn to demo menu
        else if (GameStateManager.gameStateManager.GetDemoMode() == GameStateManager.DemoOnOff.On)
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.DemoMenu);
    }

    public void ReturnToBoxerSelection()
    {
        /// Santiago Castaneda Munoz / Ring Leader August Version
        /// Adding the Functionality to go back to the main Menu

      //If Demo mode is off, return to boxer selection
        if (GameStateManager.gameStateManager.GetDemoMode() == GameStateManager.DemoOnOff.Off)
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.BoxerSelection);

      //If Demo mode is on, return to demo wait screen
        else if (GameStateManager.gameStateManager.GetDemoMode() == GameStateManager.DemoOnOff.On)
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.DemoWait);

    }
}
