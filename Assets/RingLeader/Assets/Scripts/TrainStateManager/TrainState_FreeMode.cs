﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state that allows the player to test freely 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_FreeMode : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.SetInstructions("This is free mode. Go ahead and move around and punch to your leisure!");

		Debug.Log ("TrainState_FreeMode has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{



	}

	public override void ExitState()
	{

	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{

	}
}
