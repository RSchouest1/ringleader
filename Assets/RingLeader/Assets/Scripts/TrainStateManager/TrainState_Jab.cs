﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: State to teach boxers how to jab
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_Jab : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	private int pressed = 0; //Amount of times they have pressed the required button



	public override void InitializeState()
	{
		tText.DisplayJabText ();

		Debug.Log ("TrainState_Jab has started");
	}

	public override void StartState ()
	{

	}
		
	public override void UpdateState()
	{
		MetRequirements ();

		if (boxingState.getDidJab () == true) {
			boxingState.toggleDidJab ();
			pressed++;
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
