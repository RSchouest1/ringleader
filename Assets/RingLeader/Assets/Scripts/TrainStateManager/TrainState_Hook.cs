﻿
/*
 Author: Cooper Schneider
 Date: October 2017
 Purpose: State to teach boxers how to hook 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_Hook : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayHookText ();

		Debug.Log ("TrainState_Hook has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (boxingState.getDidLeftHook()) 
		{
			boxingState.toggleDidLeftHook ();
			pressed++;
			Debug.Log ("Player has done Hook " + (pressed) + "time(s)");

		} 

		else if (boxingState.getDidRightHook()) 
		{
			boxingState.toggleDidRightHook ();
			pressed++;
			Debug.Log ("Player has done Hook " + (pressed) + "time(s)");
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
