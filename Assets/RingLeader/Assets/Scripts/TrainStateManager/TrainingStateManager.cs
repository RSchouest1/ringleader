﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Implementation of the Base training state machine
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingStateManager : MonoBehaviour {

	[SerializeField]
	private TrainingStates currentTrainState, previousTrainState;

	private List<TrainingStates> TStates = new List<TrainingStates>(); 

	//Boxer States
	public TrainState_MoveW MoveState;
	public TrainState_Jab JabState;
	public TrainState_Crouch CrouchState;
	public TrainState_FakeJab FakeState;
	public TrainState_Hook HookState;
	public TrainState_Uppercut UppercutState;
	public TrainState_CrouchJab CrouchJabState;
	public TrainState_CrouchHook CrouchHookState;
	public TrainState_CrouchUppercut CrouchUppercutState;
	public TrainState_FreeMode FreeModeState;

	//Ref States
	public TrainState_Separate SeparateState;
	public TrainState_GivePoints GivePointsState;
	public TrainState_ChooseWinner WinnerState;
	public TrainState_Disqualify DisqualifyState;
	public TrainState_StopFight StopFightState;
	public TrainState_Corners CornerState;
	public TrainState_Focus FocusState;


	//Index of the current state in the list, since it goes through one by one
	private int index = 0;

	bool changedState = false;
	bool isRef = false;

	public void RefState (){
		isRef = true;
	}

	// Use this for initialization
	void Start () {

		if (isRef) {
			TStates.Add (SeparateState);
			TStates.Add (GivePointsState);
			TStates.Add (WinnerState);
			TStates.Add (DisqualifyState);
			TStates.Add (CornerState);
			TStates.Add (FocusState);
			TStates.Add (StopFightState);
		} 
		else {
			TStates.Add (MoveState);
			TStates.Add (JabState);
			TStates.Add (CrouchState);
			TStates.Add (FakeState);
			TStates.Add (HookState);
			TStates.Add (UppercutState);
			TStates.Add (CrouchJabState);
			TStates.Add (CrouchHookState);
			TStates.Add (CrouchUppercutState);
			TStates.Add (FreeModeState);
		}

		for (int i = 0; i < TStates.Capacity; i++) 
		{
			TStates [i].enabled = false;
		}

		ChangeTrainState (MoveState);
		currentTrainState.InitializeState ();
	}


	void ChangeTrainState(TrainingStates tempState)
	{
		currentTrainState = tempState;
	}
		
	// Update is called once per frame
	void Update () 
	{

		currentTrainState.UpdateState ();

		if (changedState) 
		{
			changedState = !changedState;
			currentTrainState.InitializeState ();
		}

		else if (currentTrainState.GetCompletedTask()) 
		{
			index++;
			previousTrainState = currentTrainState;
			currentTrainState.ExitState ();
			currentTrainState = TStates[index];
			previousTrainState.enabled = false;
			currentTrainState.enabled = true;
			changedState = true;
		}
	}

	public bool GetIsRef()
	{
		return isRef;
	}
}
