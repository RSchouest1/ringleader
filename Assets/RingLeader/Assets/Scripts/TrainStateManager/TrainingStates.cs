﻿/*
 Author: Cooper Schneider
Start and End date October 9th 2017 / October 13th 2017
 Purpose: Base Training states for the training state machines
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TrainingStates : MonoBehaviour {

	[HideInInspector]
	public enum StateStatus { Initializing, Staged, Failed, Start, Update, Exit }

	public StateStatus currentStatus = StateStatus.Initializing;

	private bool completedTask = false;

	// THIS HAPPENS IN SCENE 0 IN THE BUILD ORDER
	public abstract void InitializeState(); // Anything the state needs to do at awake before the first frame of the game runtime.

	public abstract void StartState(); // anything the state needs to do when it is transitioned to from another state

	public abstract void UpdateState(); // anything the state needs to do on every frame while it is active

	public abstract void ExitState(); // anything the state needs to do when it is transitioning to another state

	public abstract void MetRequirements (); //Called in UpdateState when the Training has met it's requirements to move to the next one

	public bool GetCompletedTask()
	{
		return completedTask;
	}

	public void SetCompletedTask()
	{
		completedTask = !completedTask;
	}
}
