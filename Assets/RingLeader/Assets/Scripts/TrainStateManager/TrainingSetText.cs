﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Public methods to display the text instructions for training scenes
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrainingSetText : MonoBehaviour {

	[SerializeField]
	private Text Instructions;

	[SerializeField]
	private BoxerData1 boxData;

	public void DisplayMovementText()
	{
		if (boxData.isSouthpaw) {
			Instructions.text = "You are in Southpaw mode. Moving is all relative to the other opponent. Pressing 'W' will make you move towards the other opponent," +
			" while 'S' makes you move away and 'A' and 'D' make you move around the opponent respectively";
		} else {
			Instructions.text = "You are in Orthodox mode. Moving is based on the view of the ring. Pressing 'W' will make you move towards the top of the ring," +
				" while 'S' makes you move to the bottom and 'A' and 'D' make you move Left and Right respectively";
		}
	}

	//Displays text for instructions on how to use the Jab
	public void DisplayMoveWText(){
		Instructions.text = "Press W to move forward";
	}
	public void DisplayMoveAText(){
		Instructions.text = "Press A to move to the left";
	}
	public void DisplayMoveSText(){
		Instructions.text = "Press S to move backwards";
	}
	public void DisplayMoveDText(){
		Instructions.text = "Press D to move to the right";
	}
	public void DisplayMoveIText(){
		Instructions.text = "Press I to move forward";
	}
	public void DisplayMoveJText(){
		Instructions.text = "Press J to move to the left";
	}
	public void DisplayMoveKText(){
		Instructions.text = "Press K to move backwards";
	}
	public void DisplayMoveLText(){
		Instructions.text = "Press L to move to the left";
	}
	public void DisplayJabText()
	{
		Instructions.text = "To do a Jab press 'R' for a Left Jab and 'T' for a Right Jab.";
	}

	public void DisplayCrouchText()
	{
		Instructions.text = "To do a Crouch, press 'H'. Hold it down to keep Crouching.";
	}

	public void DisplayFakeText()
	{
		Instructions.text = "To do a Fake Jab, press 'F' for a left fake and 'G' for a right fake.";
	}

	public void DisplayHookText()
	{
		Instructions.text = "To do a Hook hold both 'F' and 'R' for a left hook or 'F' and 'T' for a right hook";
	}

	public void DisplayUppercutText()
	{
		Instructions.text = "To do an Uppercut hold both 'G' and 'R' for a left uppercut or 'G' and 'T' for a right uppercut";
	}

	public void DisplayCrouchJabText()
	{
		Instructions.text = "To do a Crouch Jab first hold 'H' and then press 'R' for a Left Jab and 'T' for a Right Jab.";
	}

	public void DisplayCrouchHookText()
	{
		Instructions.text = "To do a Crouch Hook first hold 'H' and the press both 'G' and 'T' for a hook";
	}

	public void DisplayCrouchUppercutText()
	{
		Instructions.text = "To do a Crouch Uppercut first hold 'H' and then press both 'G' and 'R' for an uppercut";
	}

	//All text methods under this comment are for Ref

	public void DisplaySeparateText()
	{
		Instructions.text = "To separate players, make sure they are close to each other and press 'Q' and '1'";
	}

	public void DisplayGivePointsText()
	{
		Instructions.text = "To give points hold 'Q' and then press '2' for player 1 and '3' for player 2";
	}

	public void DisplayCornersText()
	{
		Instructions.text = "To send players to their corners press 'C' and '1'";
	}

	public void DisplayChooseWinnerText()
	{
		Instructions.text = "To declare a winner, press 'Q' and '4'";
	}

	public void DisplayDisqualifyText()
	{
		Instructions.text = "To disqualify a player for foul play hold 'E' and press '1' for player 1 and '2' for player 2";
	}

	public void DisplayFocusText()
	{
		Instructions.text = "The Ref can go into Focus mode to see illegal hits by pressing 'C' and '2'. Toggle between players by pressing 'B'";
	}

	public void DisplayStopFightText()
	{
		Instructions.text = "To Stop the fight, press 'Q' and '5'";
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public Text GetInstructions()
	{
		return Instructions;
	}

	public void SetInstructions(string newText)
	{
		Instructions.text = newText;
	}

}
