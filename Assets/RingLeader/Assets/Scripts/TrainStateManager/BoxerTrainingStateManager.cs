﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: State Machine to control the Boxer training scene
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerTrainingStateManager : MonoBehaviour {

	[SerializeField]
	private TrainingStates currentTrainState, previousTrainState;

	private List<TrainingStates> TStates = new List<TrainingStates>(); 

	//Boxer States
	public TrainState_Jab JabState;
	public TrainState_Crouch CrouchState;
	public TrainState_FakeJab FakeState;
	public TrainState_Hook HookState;
	public TrainState_Uppercut UppercutState;
	public TrainState_CrouchJab CrouchJabState;
	public TrainState_CrouchHook CrouchHookState;
	public TrainState_CrouchUppercut CrouchUppercutState;
	public TrainState_FreeMode FreeModeState;
	public TrainState_MoveW MoveWState;
	public TrainState_MoveA MoveAState;
	public TrainState_MoveS MoveSState;
	public TrainState_MoveD MoveDState;

	//Index of the current state in the list, since it goes through one by one
	private int index = 0;

	bool changedState = false;

	// Use this for initialization
	void Start () {

		TStates.Add (MoveWState);
		TStates.Add (MoveSState);
		TStates.Add (MoveAState);
		TStates.Add (MoveDState);
		TStates.Add (JabState);
		TStates.Add (CrouchState);
		TStates.Add (FakeState);
		TStates.Add (HookState);
		TStates.Add (UppercutState);
		TStates.Add (CrouchJabState);
		TStates.Add (CrouchHookState);
		TStates.Add (CrouchUppercutState);

		TStates.Add (FreeModeState);

		ChangeTrainState (MoveWState);

		currentTrainState.InitializeState ();
	}


	void ChangeTrainState(TrainingStates tempState)
	{
		currentTrainState = tempState;
	}
		
	// Checks what the current state is and calls it's update function
	void Update () 
	{
		
		currentTrainState.UpdateState ();

		//Checks if a state has changed last frame and if it has it will cause the new states initialize method
		if (changedState) 
		{
			changedState = !changedState;
			currentTrainState.InitializeState ();
		}

		//Sees if the code in the state has completedTask set to true and then changes to the next state turning changedState on
		else if (currentTrainState.GetCompletedTask()) 
		{
			index++;
			previousTrainState = currentTrainState;
			currentTrainState.ExitState ();
			currentTrainState = TStates[index];
			previousTrainState.enabled = false;
			currentTrainState.enabled = true;
			changedState = true;
		}
	}

}
