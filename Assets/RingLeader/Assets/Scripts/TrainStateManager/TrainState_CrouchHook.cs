﻿
/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state to teach player how to do a crouch hook punch 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_CrouchHook : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayCrouchHookText ();

		Debug.Log ("TrainState_CrouchHook has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (Input.GetKey (KeyCode.H) && boxingState.getDidLeftHook ()) {
			boxingState.toggleDidLeftHook ();
			pressed++;
			Debug.Log ("Player has done Crouch Hook " + pressed + "time(s)");
		} else if (Input.GetKey (KeyCode.H) && boxingState.getDidRightHook ()) {
			boxingState.toggleDidRightHook ();
			pressed++;
			Debug.Log ("Player has done Crouch Hook " + pressed + "time(s)");
		} else if (boxingState.getDidLeftHook ()) 
		{
			boxingState.toggleDidLeftHook ();
		} else if (boxingState.getDidRightHook ()) 
		{
			boxingState.toggleDidRightHook ();
		}

	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
