﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state to teach the referee how stop the fight
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_StopFight : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	public override void InitializeState()
	{
		tText.DisplayStopFightText ();

		Debug.Log ("TrainState_StopFight has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		//TO DO: Make the if statement work based on a completed game state, done similar in the boxer train states.
		//Add them in after they are all made and added into the Referee State Manager

		if (Input.GetKey (KeyCode.Q) && Input.GetKeyDown(KeyCode.Alpha5)) 
		{
			pressed++;
			Debug.Log ("Player has Stopped the Fight " + pressed + "time(s)");
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
