﻿
/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: State to teach a boxer how to fake jab
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_FakeJab : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayFakeText ();

		Debug.Log ("TrainState_FakeJab has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (boxingState.getDidLeftFake()) 
		{
			boxingState.toggleDidLeftFake ();
			pressed++;
			Debug.Log ("Player has done Fake " + (pressed) + "time(s)");
		} 

		else if (boxingState.getDidRightFake()) {
			boxingState.toggleDidRightFake ();
			pressed++;
			Debug.Log ("Player has done Fake " + (pressed) + "time(s)");
		}
	}

	public override void ExitState()
	{
		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
