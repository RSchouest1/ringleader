﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state to teach the player how to crouch jab
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_CrouchJab : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayCrouchJabText ();

		Debug.Log ("TrainState_CrouchJab has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (Input.GetKey(KeyCode.H) && boxingState.getDidJab()) 
		{
			boxingState.toggleDidJab ();
			pressed++;
			Debug.Log ("Player has done Crouch Jab " + (pressed) + "time(s)");
		} 

		else if (boxingState.getDidJab()) {
			boxingState.toggleDidJab ();
		}
	}

	public override void ExitState()
	{
		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
