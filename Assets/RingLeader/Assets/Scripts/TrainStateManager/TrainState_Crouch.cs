﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training State to teach player how to crouch 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_Crouch : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	public override void InitializeState()
	{
		tText.DisplayCrouchText ();

		Debug.Log ("TrainState_Crouch has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (Input.GetKeyDown (KeyCode.H)) 
		{
			pressed++;
			Debug.Log ("Player has done Jab " + pressed + "time(s)");
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
