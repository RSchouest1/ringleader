﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state to teach the referee how to seperate the boxers
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_Separate : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private GameObject Player1, Player2;

	public override void InitializeState()
	{
		tText.DisplaySeparateText ();

		Debug.Log ("TrainState_Separate has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		//TO DO: Make the if statement work based on a completed game state, done similar in the boxer train states.
		//Add them in after they are all made and added into the Referee State Manager

		if (Input.GetKey(KeyCode.Q)) 
		{
			pressed++;
			Debug.Log ("Player has Separated " + pressed + "time(s)");
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}

	void Start()
	{
		
	}
}
