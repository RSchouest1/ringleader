﻿
/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training State to teach the player how to crouch uppercut
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_CrouchUppercut : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayCrouchUppercutText ();

		Debug.Log ("TrainState_CrouchUppercut has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (Input.GetKey (KeyCode.H) && boxingState.getDidLeftUppercut ()) {
			boxingState.toggleDidLeftUppercut ();
			pressed++;
			Debug.Log ("Player has done Crouch Uppercut " + pressed + "time(s)");
		} 
		else if (Input.GetKey (KeyCode.H) && boxingState.getDidRightUppercut ()) {
			boxingState.toggleDidRightUppercut ();
			pressed++;
			Debug.Log ("Player has done Crouch Uppercut " + pressed + "time(s)");
		} 
		else if (boxingState.getDidLeftUppercut ()) {
			boxingState.toggleDidLeftUppercut ();
		}
		else if (boxingState.getDidRightUppercut ()) {
			boxingState.toggleDidRightUppercut ();
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >= 3) 
		{
			SetCompletedTask ();
		}
	}
}
