﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: State Machine to control the Referee training scene
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefTrainingStateManager : MonoBehaviour {

	[SerializeField]
	private TrainingStates currentTrainState, previousTrainState;

	private List<TrainingStates> TStates = new List<TrainingStates>(); 

	public TrainState_FreeMode FreeModeState;

	//Ref States
	public TrainState_Separate SeparateState;
	public TrainState_GivePoints GivePointsState;
	public TrainState_ChooseWinner WinnerState;
	public TrainState_Disqualify DisqualifyState;
	public TrainState_StopFight StopFightState;
	public TrainState_Corners CornerState;
	public TrainState_Focus FocusState;
	public TrainState_MoveI MoveIState;
	public TrainState_MoveK MoveKState;
	public TrainState_MoveJ MoveJState;
	public TrainState_MoveL MoveLState;


	//Index of the current state in the list, since it goes through one by one
	private int index = 0;

	bool changedState = false;

	// Use this for initialization
	void Start () {

		TStates.Add (MoveIState);
		TStates.Add (MoveKState);
		TStates.Add (MoveJState);
		TStates.Add (MoveLState);
		TStates.Add (SeparateState);
		TStates.Add (GivePointsState);
		TStates.Add (WinnerState);
		TStates.Add (DisqualifyState);
		TStates.Add (CornerState);
		TStates.Add (FocusState);
		TStates.Add (StopFightState);

		TStates.Add (FreeModeState);

		ChangeTrainState (MoveIState);

		currentTrainState.InitializeState ();
	}


	void ChangeTrainState(TrainingStates tempState)
	{
		currentTrainState = tempState;
	}

	// Checks what the current state is and calls it's update function
	void Update () 
	{

		currentTrainState.UpdateState ();

		//Checks if a state has changed last frame and if it has it will cause the new states initialize method
		if (changedState) 
		{
			changedState = !changedState;
			currentTrainState.InitializeState ();
		}

		//Sees if the code in the state has completedTask set to true and then changes to the next state turning changedState on
		else if (currentTrainState.GetCompletedTask()) 
		{
			index++;
			previousTrainState = currentTrainState;
			currentTrainState.ExitState ();
			currentTrainState = TStates[index];
			previousTrainState.enabled = false;
			currentTrainState.enabled = true;
			changedState = true;
		}
	}
		
}
