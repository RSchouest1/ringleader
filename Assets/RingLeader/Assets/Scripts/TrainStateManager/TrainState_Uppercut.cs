﻿/*
 Author: Cooper Schneider
 Start and End date October 9th 2017 / October 13th 2017
 Purpose: Training state to teach the boxer how to uppercut
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainState_Uppercut : TrainingStates {

	[SerializeField]
	private TrainingSetText tText;

	private int pressed = 0;

	[SerializeField]
	private BoxerStateMachine1 boxingState;

	public override void InitializeState()
	{
		tText.DisplayUppercutText ();

		Debug.Log ("TrainState_Uppercut has started");
	}

	public override void StartState ()
	{

	}

	public override void UpdateState()
	{

		MetRequirements ();

		if (boxingState.getDidLeftUppercut()) 
		{
			boxingState.toggleDidLeftUppercut ();
			pressed++;
			Debug.Log ("Player has done Uppercut " + (pressed) + "time(s)");
		} 

		else if (boxingState.getDidRightUppercut()) 
		{
			boxingState.toggleDidRightUppercut ();
			pressed++;
			Debug.Log ("Player has done Uppercut " + (pressed) + "time(s)");
		}
	}

	public override void ExitState()
	{

		SetCompletedTask ();
	}

	//Called in Update every frame to see if the requirements for this lesson have been met to go to the next
	public override void MetRequirements()
	{
		if (pressed >=3) 
		{
			SetCompletedTask ();
		}
	}
}
