﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideLineRopeRenderer : MonoBehaviour {

	public Transform[] pointLinks;
	public LineRenderer lineRenderer;

	// Use this for initialization
	void Start () {
		lineRenderer.positionCount = pointLinks.Length;
	}
	
	// Update is called once per frame
	void Update () 
	{
		for(int i = 0; i < pointLinks.Length; ++i)
		{
			lineRenderer.SetPosition(i, pointLinks[i].position);
		}
	}
}
