﻿/*
Author: John Hartzell
Date Modified: 7/26/17
Credit:
Purpose: This is a static scene manager script that can be called from any class in the game.
Worked on by: Eduardo Sarmiento.


Hector Castelli Zacharias
Date Modified: 11/15/17
Purpose: Hotplug for gameover scene so the objects don't get deleted and the game over screen can see the data.
*/

using UnityEngine.SceneManagement;
using UnityEngine;

public class RingLeaderSceneManager : MonoBehaviour {

    public static RingLeaderSceneManager instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }

        Debug.Log(instance);
    }

    public void ChangeScene(int sceneID)
    {
        if(SceneManager.GetSceneByBuildIndex(sceneID) != null)
        {
            //Hotplug for the boxers stats
            if (sceneID == 5) //Current id for the Game Over scene
            {
                SceneManager.LoadScene(sceneID, LoadSceneMode.Additive);
                return;
            }
            SceneManager.LoadScene(sceneID);
        }
    }

}
