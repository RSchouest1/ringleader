﻿//Updater: Brandon Vitayanuvatti
//Disclaimer/Purpose: I AM NOT THE ORIGNAL AUTHOR OF THIS SCRIPT. The original author did not put their name in the header.
//I'm merely changing 5.0f into a variable called maxTime so I can access it better from FocusBar.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FocusMode_Panel : MonoBehaviour {

    private float trackTime;
    public float TrackTime() { return trackTime; }

    private Color defaultcolor = Color.white;

    public string _Tag;

    RefereeStateMachine Focusm;

    // For future reference
    // Q means Referee...
    public GameObject Q;

    //Brandon: Making these two variables public because when I tried to assign them as privates they would not assign properly.
    public GameObject focusBar;
    public FocusBar focusBarScript;


    private Dictionary<changeStates, Action> fsm = new Dictionary<changeStates, Action>();

    enum changeStates
    {
        ON,
        OFF,

        NUM_STATES
    }

    private changeStates curState = changeStates.ON;

    
    void Start()
    {
        Focusm = Q.GetComponent<RefereeStateMachine>();//access to another script
        fsm.Add(changeStates.ON, new Action(StateOn));
        fsm.Add(changeStates.OFF, new Action(StateOFF));
        
        ResetTimeSinceLastTransition();

    }

    void SetState(changeStates newState)
    {
        curState = newState;
    }

   
    void Update()
    {
        
        fsm[curState].Invoke();
        trackTime += Time.deltaTime;
        focusBarScript.decrementTimer();
    }

    private void OnTriggerEnter(Collider other)
    {
      
        if (other.tag == _Tag)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;

            ResetTimeSinceLastTransition();
            SetState(changeStates.ON);
          
        }
    }

    void StateOn()
    {
        if (Focusm.checking == true)
        {
            ResetTimeSinceLastTransition();
            SetState(changeStates.OFF);
        }

        else
            if (trackTime > focusBarScript.maxTime )
            {
                ResetTimeSinceLastTransition();
                gameObject.GetComponent<Renderer>().material.color = defaultcolor;

            }

             
    }

    void StateOFF()
    {
        if (trackTime > focusBarScript.maxTime)
        {
            gameObject.GetComponent<Renderer>().material.color = defaultcolor;
            ResetTimeSinceLastTransition();
           
        }
    }



    public void ResetTimeSinceLastTransition()
    {
        trackTime = 0;
    }

}

