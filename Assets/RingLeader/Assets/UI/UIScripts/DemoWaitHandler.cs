﻿/*------------------------------------------------------------------------------------------------
Original Author: Alex Ault
Date: 11/6/17
Credit:
Purpose: This script handles the input and subsequent loading of the BoutCOnditions scene in the DemoWait scene.
Worked on by:
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoWaitHandler : MonoBehaviour
{
  //Variables for ready up
    bool pOneReady = false;
    bool pTwoReady = false;

  //Variable for players chosen modes
    bool pOneOrthodox = true; //True = Orthodox, False = Southpaw
    bool pTwoOrthodox = false; //True = Orthodox, False = Southpaw

    //Variables for the four ready/unready text objects
    public GameObject P1R;
    public GameObject P1NR;
    public GameObject P2R;
    public GameObject P2NR;

    //Variables for the each player's type text objects
    public GameObject P1Orth;
    public GameObject P1South;
    public GameObject P2Orth;
    public GameObject P2South;

  //Turn off the ready texts on Start
    private void Start()
    {
        P1R.SetActive(false);
        P2R.SetActive(false);
        P1South.SetActive(false);
        P2Orth.SetActive(false);
        Debug.Log("P1 Orthodox");
        Debug.Log("P2 Southpaw");
    }

    private void Update()
    {
    
        //P1 ready on pressing A
        if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("P1 ready");
            pOneReady = true;
            P1NR.SetActive(false);
            P1R.SetActive(true);
        }

        //P1 no longer ready after unpressing A
        if (Input.GetKeyUp(KeyCode.Joystick1Button0) || Input.GetKeyUp(KeyCode.T))
        {
            Debug.Log("P1 unready");
            pOneReady = false;
            P1R.SetActive(false);
            P1NR.SetActive(true);
        }

        //P2 ready on pressing A
        if (Input.GetKeyDown(KeyCode.Joystick2Button0) || Input.GetKeyDown(KeyCode.Semicolon))
        {
            Debug.Log("P2 ready");
            pTwoReady = true;
            P2NR.SetActive(false);
            P2R.SetActive(true);
        }

        //P2 no longer ready after unpressing A
        if (Input.GetKeyUp(KeyCode.Joystick2Button0) || Input.GetKeyUp(KeyCode.Semicolon))
        {
            Debug.Log("P2 unready");
            pTwoReady = false;
            P2R.SetActive(false);
            P2NR.SetActive(true);
        }


      //If P1 hits horizontal direction, toggle between Orth and South
        if ((Input.GetKeyDown(KeyCode.D) || Input.GetAxis("Red_Horizontal") > 0) && pOneOrthodox)
        {
            pOneOrthodox = false;
            P1Orth.SetActive(false);
            P1South.SetActive(true);
            Debug.Log("P1 Southpaw");
        }

        else if ((Input.GetKeyDown(KeyCode.A) || Input.GetAxis("Red_Horizontal") < 0) && !pOneOrthodox)
        {
            pOneOrthodox = true;
            P1South.SetActive(false);
            P1Orth.SetActive(true);
            Debug.Log("P1 Orthodox");
        }

        //If P2 hits horizontal direction 
        if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxis("Blue_Horizontal") > 0) && pTwoOrthodox)
        {
            pTwoOrthodox = false;
            P2Orth.SetActive(false);
            P2South.SetActive(true);
            Debug.Log("P2 Southpaw");
        }

        else if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxis("Blue_Horizontal") < 0) && !pTwoOrthodox)
        {
            pTwoOrthodox = true;
            P2South.SetActive(false);
            P2Orth.SetActive(true);
            Debug.Log("P2 Orthodox");
        }


        //If both players holding A: set both players to Eli, send chosen movement modes to the GSM, load BoutConditions
        if (pOneReady && pTwoReady)
        {
            GameStateManager.gameStateManager.AllCharactersData.ChooseEli();

            if(pOneOrthodox)
                GameObject.Find("GameStateManager").GetComponent<GameStateManager>().SetToggleRedToFalse();

            else if(!pOneOrthodox)
                GameObject.Find("GameStateManager").GetComponent<GameStateManager>().SetToggleRedToTrue();

            if(pTwoOrthodox)
                GameObject.Find("GameStateManager").GetComponent<GameStateManager>().SetToggleBlueToFalse();

            else if(!pTwoOrthodox)
                GameObject.Find("GameStateManager").GetComponent<GameStateManager>().SetToggleBlueToTrue();

            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.BoutConditions);
        }
    }

}
