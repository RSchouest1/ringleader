﻿/*------------------------------------------------------------------------------------------------
Original Author: Alex Ault
Date: 11/15/17
Credit:
Purpose: This scripts finds and retrieves each boxers' stats from their respective stat managers
Worked on by:
------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverStatsFetcher : MonoBehaviour
{
  //Variables for Red boxer
    public Text rJabs;
    public Text rCrosses;
    public Text rHooks;
    public Text rUppercuts;
    public Text rKnockOuts;
    public Text rIllegals;

  //Variables for Blue boxer
    public Text bJabs;
    public Text bCrosses;
    public Text bHooks;
    public Text bUppercuts;
    public Text bKnockOuts;
    public Text bIllegals;

    private void Start()
    {
      //Functions to fetch Red boxer's stats
        rJabs.GetComponent<Text>().text += " "+ GameObject.Find("Boxer_Red").GetComponent<StatsManager>().jabsLanded.ToString();
        rCrosses.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Red").GetComponent<StatsManager>().crossesLanded.ToString();
        rHooks.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Red").GetComponent<StatsManager>().hooksLanded.ToString();
        rUppercuts.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Red").GetComponent<StatsManager>().uppercutsLanded.ToString();
        rKnockOuts.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Red").GetComponent<StatsManager>().knockdowns.ToString();
        rIllegals.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Red").GetComponent<StatsManager>().illegalPunchesLanded.ToString();
        GameObject.Find("Boxer_Red").GetComponent<StatsManager>().ResetStats();

        //Functions to fetch Blue boxer's stats
        bJabs.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().jabsLanded.ToString();
        bCrosses.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().crossesLanded.ToString();
        bHooks.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().hooksLanded.ToString();
        bUppercuts.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().uppercutsLanded.ToString();
        bKnockOuts.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().knockdowns.ToString();
        bIllegals.GetComponent<Text>().text += " " + GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().illegalPunchesLanded.ToString();
        GameObject.Find("Boxer_Blue").GetComponent<StatsManager>().ResetStats();



      //Unload the old scene after fetching the information
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(4);
    }
}
