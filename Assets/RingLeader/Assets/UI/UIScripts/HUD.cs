﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
//------------------------------------------------------------------------------------------------
// Author: Denzyl Fontaine
// Date: 8/13/2017
// Credit: Tiffani Koczenasz
// Credit:
// Purpose: This displays the timer and the round number in the HUD in the lower right corner. 
//			Added: Displays the counter of how many times the boxers have gone down to the HUD - Tiffani Koczenasz
//------------------------------------------------------------------------------------------------

//Author:Kevin Ayad
//Date: 10/13/2017
//Credit: Kevin Ayad
//Purpose: Corner Mechanics

public class HUD : MonoBehaviour {
    //These will hold the the values from the Game_Timer Script 
    private int Round_Number;
    private float Round_Time;
	private float Break_Time;

	//Grabs the red/blue boxer int amount from the GameStateManager script
	private int RedBoxer; 
	private int BlueBoxer;

    //These will contain the text thats supposed to be displayed
    public Text RoundNumber;
    public Text RoundTimer;
	public Text BreakTimer;

	//Displays the text for Red/Blue boxer down to screen
	public Text RedBoxerDown; 
	public Text BlueBoxerDown;

    //These will hold the curStamina From both boxers 
    private float RedStamina;
    private float BlueStamina; 

	// reference to Game_Timer script
	private Game_Timer gameTimer;

	// Use this for initialization
	void Start () {

		gameTimer = FindObjectOfType<Game_Timer> ();
    }
	
	// Update is called once per frame
	void Update () {
        
        //Round Info 
        //Grab the timer and Round number from the Game_Timer object then Access the Game_Timer Script 
        Round_Number = GameStateManager.gameStateManager.roundActionGameState.GetCurrentRound();
        Round_Time = GameStateManager.gameStateManager.boutTimer.GetRoundClock();
		Break_Time = GameStateManager.gameStateManager.boutTimer.GetBreakClock();

		//Down Count Info
		//Grabs the int from GameStateManager - that was changed from BoxerState_KnockedDown
		BlueBoxer = GameStateManager.gameStateManager.blueBoxer;
		RedBoxer = GameStateManager.gameStateManager.redBoxer;

        //Convert the Round_Time into minutes and seconds
        string minutes = Mathf.Floor(Round_Time / 59f).ToString("00");
        string seconds = (Round_Time % 59f).ToString("00");
        
		// Converts the Break_Time into minutes and secons
		string breakMinutes = Mathf.Floor(Break_Time / 59f).ToString("00");
		string breakSeconds = (Break_Time % 59f).ToString("00");

        //Create the message that you want to display to the hud 
        RoundNumber.text = "Round " + Round_Number +" out of " + GameStateManager.gameStateManager.boutConditionsGameState.SetMaxRound();
        RoundTimer.text = ""+ minutes + ":" + seconds;
		BreakTimer.text = ""+ breakMinutes + ":" + breakSeconds;

		//Display count to hud
		RedBoxerDown.text = "" + RedBoxer;
		BlueBoxerDown.text = "" + BlueBoxer;


        //Stamina Info
        //Grab the curStamina from the boxers 
        RedStamina = GameStateManager.gameStateManager.BoxerInput.GetComponent<Stamina>().GetCurStamina();
        BlueStamina = GameStateManager.gameStateManager.BoxerInput_2.GetComponent<Stamina>().GetCurStamina();

        //check to see how much stamina each fighter has then display the coaching tips
        if (RedStamina <= 4.0f && BlueStamina <= 4.0f)
        {
            DisplayStaminaCoaching();
        }

		// if in play, show round timer and hide break timer
		if(gameTimer.inPlay){

			RoundTimer.enabled = true;
			BreakTimer.enabled = false;
		}

		// if not in play, show break timer and hide round timer
		else{

			RoundTimer.enabled = false;
			BreakTimer.enabled = true;
		}
    }

    void DisplayStaminaCoaching()
    {

    }

    void DisplayBlockCoaching()
    {

    }

 	
}
