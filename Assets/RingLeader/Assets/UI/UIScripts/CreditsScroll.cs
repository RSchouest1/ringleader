﻿//------------------------------------------------------------------------------------------------
// Author: Tiffani Koczenasz
// Date: 9/24/2017
// Credit: 
// Credit:
// Purpose: This will display the Credits for the game. The credits will scroll up the screen 
//			until all credits have been displayed.
//------------------------------------------------------------------------------------------------
// Updated by: Luke G Gillen
// Date: 11/13/17
// Purpose: Updating credits to include all contributors for the demo build(11/15/17)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsScroll : MonoBehaviour
{

    public Text Credits;
    public Scrollbar scrollBar;
    public ScrollRect scrollRect;

    public void Update()
    {

        //Scrolls the rect downwards to value of 0
        scrollRect.velocity = new Vector2(0f, 100f);

        //TODO: Adjust Height of the CreditsText as names are added. 
        //				creditsPanel > ScrollView > Viewport > CreditsText
        //
        //		Replace back button - once all credits have been displayed, switch back to the main menu. 



        //***Important***
        //    Whenever you edit the length of the credits, make sure that it does not break the credits UI.
        //    If the Credits are too long or if they begin half way down, you will need to edit the length(Height) of the credits,
        //    as well as the Y position of the credits on the canvas.
        //    CreditsPanel > ScrollView > Viewport > CreditsText
        //    Credits are updated as of 11/15/17
        //    Luke G Gillen

        // The "@" is being replaced by \n, 
        //See end of credits.text, .Replace("@","\n")
        Credits.text =
            ("\n" +
            "\n" +
            "\n <b>Ring Leader</b>@@@" +
            "\n" +
            "\n" +
            "\n" + "<b>Creative Director/Design/PO</b>" +
            "\n" + "Keyvan Acosta" + "@"+

            "\n" + "<b>PRODUCTION</b>" + "@" +
            "John Hartzell(SM)@"+
            "Tyler Fronczak(SM)@" +
            "Richard Altreche(SM)@"+
            "Frank Cossel(SM)  @"+ "@"+

            "<b>Leads</b>@"+
            "Luke Gillen@@"+

            "\n<b>Production Assistants</b>" +
            "\n" + "<i>Fighting Dynamics</i>@"+
            "Scottie Newman - Fighting Dynamics@"+
            "Bailey Fitzgibbon - Referee Dynamics@@"+
            "<i>Prototypes</i>@"+
            "Joshua Claassen - Prototype Statistics@@"+
            "<i>Animation </i>@"+
            "Michael Buck - Preliminary Animation@"+
            "Juan Sanchez - Rigging@@"+
            "<i>Statistics</i>@"+
            "Alex Yaws - statistics integration KOP@@"+
            "<b>SCRIPTING (UNITY3D)</b>@"+
            "<i><b>Systems</b></i>@"+
            "<i>Backend State Machine</i>@"+
            "John Hartzel @" +
            "Santiago Castañeda Muñoz@@"+
            "<i>Gameplay</i>@" +
            "<i><b>Boxers</b></i>@"+
            "<i>Blocking, Hit Reactions, @Illegal Punches, Crouching</i>@" +
            "Daniel Vanallen@@" +
            "<i>Animation Blending</i>@" +
            "Hector Castelli Zacharias@@" +
            "<i>Fighting States</i>@" +
            "Cheng An Lin@@" +
            "<i>Punches</i>@" +
            "Yikun Guo@@" +
            "<b><i>Referee</i></b>@" +
            "<i>Focus Mode</i>@" +
            "Lei Quan@" +
            "Yi Li@@" +
            "<i>General</i>@" +
            "Timothy Cable@" +
            "Yi Li@@" +
            "<b>Data Design</b>@" +
            "Joshua Claassen - KOP@@" +
            "<i><b>Demo and Prototypes</b></i>@" +
            "Alex Ault - Demo Mode@" +
            "Hunter Hempstead - Boxer Prototype@@" +
            "<b><i>Writing</i></b>@" +
            "<b>Production Documentation</b>@" +
            "<i>Technical / Code</i>@" +
            "John Hartzel@@" +
            "<i>Design</i>@" +
            "Luke Gillen@@" +
            "<b>Research</b>@" +
            "Kevin Hellmuth - Boxer Dynamics@@" +
            "<b>Character(s)</b>@@" +
            "<i>Statistics</i>@" +
            "Joshua Claassen@" +
            "Efrain Suarez@@" +
            "<b>Boxers</b>@" +
            "<i>Referee</i>@" +
            "Jeremy Passmore@" +
            "Timothy Cable@" +
            "Antonio Perez@" +
            "<i>Boxer</i>@" +
            "Christopher Donahue - Boxer@" +
            "Kevin Hellmuth - Boxer@" +
            "<i>Animations</i>@"+
            "Brandon Vitayanuvatti@@" +
            "<b>Scripting</b>@" +
            "Hector Castelli Zacharias - Movement Animation Blend@@" +
            "Alex Ault - Demo Mode@@" +
            "Daniel Vanallen - Boxer Hit Reactions, Illegal Punches,@ Crouching, Blocking@@" +
            "Brandon Vitayanuvatti - Referee Animations,@ Referee Focus Mode, Referee Controls@@" +
            "John Hartzell - Technical Architecture (State Machine, combos)@@" +
            "Tyler Fronczak - Technical Architecture (Animations, Character Classes)@@" +
            "Timothy Cable - Game Setup@@" +
            "Hunter Hempstead - Boxer Prototype@@" +
            "Kevin Hellmuth - Boxer Dynamics@@" +
            "Joshua Claassen - KOP@@" +
            "Nicholas O'keeffe - Input@@" +
            "Mitchell Ford- Combo setup@@" +
            "Eric Wimpee - Collision@@" +
            "Eduardo Sarmiento - Game State Machine@@" +
            "Juan Sanchez - Animation Tool@@" +
            "Christian Cipriano - Referee State Machine Integration@@" +
            "Blakeney Pittman - Animation Integration@@" +
            "<b>SPECIAL THANKS</b>@" +
            "Alexander Yaws@" +
            "Antonio Perez@" +
            "Ashley Horne@" +
            "Bailey Fitzgibbon@" +
            "Benjamin Ragan@" +
            "Blakeny Pittman@" +
            "Branden@" +
            "Casey A Howard@" +
            "Chandler Bockes@" +
            "Cheng An Lin@" +
            "Christian Cipriano@" +
            "Christopher Donohue@" +
            "Cody Sneed@" +
            "Dai Nguyen@" +
            "Dang Nguyen@" +
            "Daniel Coyne@" +
            "Denzyl Fontaine@" +
            "Douglas Chamberlain@" +
            "Eduardo  Sarmiento@" +
            "Efrain Suarez@" +
            "Ellis Hernandez@" +
            "Emilio Mojica@" +
            "Eric Wimpee@" +
            "Gabe A Robin@" +
            "Hunter Hempstead@" +
            "Jacob May@" +
            "Jeremy Bello@" +
            "JK Williams@" +
            "Joel Knight@" +
            "Jon Conn@" +
            "Joseph Garrity@" +
            "Joshua Claassen@" +
            "JP More@" +
            "Karina Heizer@" +
            "Kevin Hellmuth@" +
            "Lei Q@" +
            "Luke Gillen@" +
            "Mehmet Holbert@" +
            "Micha Young@" +
            "Michael Deininger@" +
            "Mitchell Ford@" +
            "MN Zeng@" +
            "Nicholas O'Keefe@" +
            "Riley Thompson@" +
            "RM Coberly@" +
            "Rowan Anderson@" +
            "Santiago Castaneda Munoz@" +
            "Sven Zobler@" +
            "Tamago Gray@" +
            "Tiffani Koczenasz@" +
            "Tim Cable@" +
            "T kirkland@" +
            "Yi Li@" +
            "Yikun Guo@" +
            "Zach Schram@" +
            "Alex Ault@" +
            "Azier Larranaga@" +
            "Cooper Schneider@" +
            "James Dickson@" +
            "Jordan Ferris@" +
            "Kevin Ayad@" +
            "Santiago Castañeda Muñoz@" +
            "Timothy Fownler@" +
            "Zahary Stancliffe@" +
            "Bishop Allen@@@" +
            "\n").Replace("@", "\n");
        
    } //End Update
} //End CreditsScroll
