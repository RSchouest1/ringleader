﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ThreeCharManager : MonoBehaviour
{
    [SerializeField] Button[] Button0;  // Setting the button to this variable 
    [SerializeField] Button[] Button1;
    [SerializeField] Button[] Button2;
    [SerializeField] Button Ready;
    bool[] buttonPressed = new bool[3] { false, false, false };
    [SerializeField] GameObject[] playerControler; // Setting this to the controllers at the bottom of the screen
    bool[] player = new bool[3] { true, true, true }; // Storing the controllers as the respective player number bool
    int currentOrder = -1;
    int[] current0Button = new int[3];
    int[] current1Button = new int[3];
    int[] current2Button = new int[3];
    bool[] locked = new bool[3] { false, false, false };

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < Button0.Length; i++) // For loop to set all the Button arrays to not interactable
        {
            Button0[i].interactable = false;
            Button1[i].interactable = false;
            Button2[i].interactable = false;
        }
        Ready.interactable = false;
    }

    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MainMenu();
        }

        if (locked[0] && locked[1] && locked[2] == true)
        {
            Ready.interactable = true;
        }
        if (!locked[0])
        {
            if (player[0] && Input.GetKeyDown(KeyCode.A))
            {
                Player0InputDown();
               
            }
            else if (player[0] && Input.GetKeyDown(KeyCode.Q))
            {
                Player0InputUp();
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                locked[0] = !locked[0];
            }
        }
        if (!locked[1])
        {
            if (player[1] && Input.GetKeyDown(KeyCode.G))
            {
                Player1InputDown();
               
            }
            else if (player[1] && Input.GetKeyDown(KeyCode.T))
            {
                Player1InputUp();
            }
            else if (Input.GetKeyDown(KeyCode.H))
            {
                locked[1] = !locked[1];
            }
        }
        if (!locked[2])
        {
            if (player[2] && Input.GetKeyDown(KeyCode.DownArrow))
            {
                Player2InputDown();
                
            }
            else if (player[2] && Input.GetKeyDown(KeyCode.UpArrow))
            {
                Player2InputUp();
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                locked[2] = !locked[2];
            }
        }
    }

    public void Player0InputDown()
    {
        if (!buttonPressed[0]) // And buttons have not been intereacted with til now
        {
            buttonPressed[0] = true; // The previous fact becomes untrue
            current0Button[0] = 0;
            Button0[current0Button[0]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player0Down();// Call this function to transition down
        }
    }
    public void Player0InputUp()
    {
    
        if (!buttonPressed[0]) // And buttons have not been intereacted with til now
        {
            buttonPressed[0] = true; // The previous fact becomes untrue
            current0Button[0] = 2;
            Button0[current0Button[0]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player0Up(); // Call this function to transition down
        }
    }
    public void Player1InputDown()
    {
        if (!buttonPressed[1]) // And buttons have not been intereacted with til now
        {
            buttonPressed[1] = true; // The previous fact becomes untrue
            current1Button[1] = 0;
            Button1[current1Button[1]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player1Down();// Call this function to transition down
        }
    }
    public void Player1InputUp()
    {

        if (!buttonPressed[1]) // And buttons have not been intereacted with til now
        {
            buttonPressed[1] = true; // The previous fact becomes untrue
            current1Button[1] = 2;
            Button1[current1Button[1]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player1Up(); // Call this function to transition down
        }
    }
    public void Player2InputDown()
    {
        if (!buttonPressed[2]) // And buttons have not been intereacted with til now
        {
            buttonPressed[2] = true; // The previous fact becomes untrue
            current2Button[2] = 0;
            Button2[current2Button[2]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player2Down();// Call this function to transition down
        }
    }
    public void Player2InputUp()
    {

        if (!buttonPressed[2]) // And buttons have not been intereacted with til now
        {
            buttonPressed[2] = true; // The previous fact becomes untrue
            current2Button[2] = 2;
            Button2[current1Button[2]].interactable = true; // Set the Demo button to interactable
        }
        else // Or if buttons have already been pressed
        {
            Player2Up(); // Call this function to transition down
        }
    }
    public void Player0Up()
    {
        if(current0Button[0] >= 1)
        {
            current0Button[0] = current0Button[0] - 1;
            Button0[current0Button[0]].interactable = true;
            Button0[current0Button[0]+1].interactable = false; 
            
        }
    }
    public void Player0Down()
    {
        if(current0Button[0] < Button0.Length - 1)
        {
           
            current0Button[0] = current0Button[0] + 1;
            Button0[current0Button[0]].interactable = true;
            Button0[current0Button[0] - 1].interactable = false;
        }
    }
    public void Player1Up()
    {
        if (current1Button[0] >= 1)
        {
            current1Button[0] = current1Button[0] - 1;
            Button1[current1Button[0]].interactable = true;
            Button1[current1Button[0] + 1].interactable = false;

        }
    }
    public void Player1Down()
    {
        if (current1Button[0] < Button0.Length - 1)
        {
            
            current1Button[0] = current1Button[0] + 1;
            Button1[current1Button[0]].interactable = true;
            Button1[current1Button[0] - 1].interactable = false;
        }
    }
    public void Player2Up()
    {
        if (current2Button[0] >= 1)
        {
            current2Button[0] = current2Button[0] - 1;
            Button2[current2Button[0]].interactable = true;
            Button2[current2Button[0] + 1].interactable = false;

        }
    }
    public void Player2Down()
    {
        if (current2Button[0] < Button0.Length - 1)
        {
          
            current2Button[0] = current2Button[0] + 1;
            Button2[current2Button[0]].interactable = true;
            Button2[current2Button[0] - 1].interactable = false;
        }
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(14);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(12);
    }
}
