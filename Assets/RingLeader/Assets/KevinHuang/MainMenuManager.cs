﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenuManager : MonoBehaviour
{
    [SerializeField] Button[] Button;  // Setting the button to this variable 
    bool buttonPressed = false;
    [SerializeField] GameObject[] playerControler; // Setting this to the controllers at the bottom of the screen
    bool[] player = new bool[3] { false, false, false }; // Storing the controllers as the respective player number bool
    int currentOrder = -1;
    int currentButton;
    void Start()
    {
        for (int i = 0; i < playerControler.Length; i++) // For loop to set all the playerControllers to inactive
        {
            playerControler[i].SetActive(false);
        }
        for (int i = 0; i < Button.Length; i++) // For loop to set all the Buttons[] to not interactable
        {
            Button[i].interactable = false;
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        DetectInput();
    }

    public void DetectInput() // Call this in update to get any input
    {

        if (Input.GetKeyDown(KeyCode.Return)) // if enter is being pressed
        {
            if (currentOrder < playerControler.Length - 1)
            {
                currentOrder++; // Universal counter gets updated
                print(currentOrder);
                player[currentOrder] = true; // player array Universal number gets turned true
                ActivePlayers(); // Calls another function for registering the controler
                print(player[currentOrder]);
            }
           
        }
        if (player[0] && Input.GetKeyDown(KeyCode.DownArrow)) // If player one presses up
        {
            if (!buttonPressed) // And buttons have not been intereacted with til now
            {
                buttonPressed = true; // The previous fact becomes untrue
                currentButton = 0;
                Button[currentButton].interactable = true; // Set the Demo button to interactable
                Button[currentButton + 1].interactable = false; // Set the Control button to inactive
            }
            else // Or if buttons have already been pressed
            {
                ButtonDown(); // Call this function to transition down
            }
        }
        else if (player[0] && Input.GetKeyDown(KeyCode.UpArrow)) // If player one decides to press the up key
        {
            if (!buttonPressed) // and the buttons have not been interacted with up til now
            {
                buttonPressed = true; // it becomes interacted with
                currentButton = 1;
                Button[currentButton - 1].interactable = false; // same as up
                Button[currentButton].interactable = true; // same as up
            }
            else // same as DownArrow
            {
                ButtonUp(); // same as DownArrow
            }
        }
        else if (player[0] && Input.GetKeyDown(KeyCode.Space) && Button[1].interactable == true) // Primary player is pressing space and Button array 1 is interactable
        {
            SceneManager.LoadScene(13);
        }
        else if (player[0] && player[1] && player[2] && Input.GetKeyDown(KeyCode.Space) && Button[0].interactable == true) // If three players are detected, and primary player presses space, Button array 0 is interactable
        {
            SceneManager.LoadScene(10);
        }
        else if(player[0] && player[1] && Input.GetKeyDown(KeyCode.Space) && Button[0].interactable == true) // If there are only two players and Button array 0 is interactable
        {
            SceneManager.LoadScene(11);
        }
    }
    public void ActivePlayers()
    {
        playerControler[currentOrder].SetActive(true); // Indicates how many players are active based off the Universal int currentOrder
    }
    public void ButtonUp()
    {
        if (currentButton >= 1)
        {
            currentButton = currentButton - 1;
            Button[0].interactable = true;
            Button[currentButton + 1].interactable = false;
        }
    }
    public void ButtonDown()
    {
        if (currentButton < Button.Length - 1)
        {
            currentButton = currentButton + 1;
            Button[currentButton - 1].interactable = false;
            Button[currentButton].interactable = true;
        }
    }
    
}
