﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Script by James Dickson
 * Feature: Different Characters
 * Start and End date October 9th 2017 / October 13th 2017
 * Notes: I created everything here unless stated otherwise*/

    /*
     * Alteration by Hector Castelli Zacharias so the object doesn't become DontDestroyOnLoad
     */

public class All_Characters : MonoBehaviour {


	private GameObject Boxer;

    // Only Stats of the boxer go into this structure

		//punch speed
		//public int mySpeed; NOT ATTACHED TO BOXER
		//Max stamina
		public int myMaxStamina;   
		//Left punch stamina cost
		public int leftpunchCost; 
		//Right punch stamina cost
		public int rightpunchCost; 
		//damage recieved from being hit while blocking
		//public int blockImpact; BLOCKING NOT FULLY WORK SO STAT CANT WORK
		//bonus damage in addition to each hit recieved
		//public int bonusincomingDamage;//KOP + int NOT WORKING PROPERLY BECAUSE PUNCH DAMAGE CANT BE ACCESSED
		//Max KOP that can be taken
		public int myMaxKOP;
		//Current KOP
		public int currentKOP; 
		//How long you regen stamina for
		public int myStaminaRegen;


    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	void Update () {
        //DontDestroyOnLoad(gameObject);
	}

    

	// All voids below are all of the various boxers and their stats
	//Heavy Weight Start
	public void ChooseEli (){

        FindObjectOfType<GameStateManager>();

		//mySpeed = 6; 
        myMaxStamina = 50; 
        myStaminaRegen = 5;
        leftpunchCost = 7;
        rightpunchCost = 4;
		//blockImpact = 2;
		//bonusincomingDamage = 1;
		myMaxKOP = 75;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
		//DecideStats (); 

	}

	public void ChooseJones (){

		//.mySpeed = 5;
		myMaxStamina = 50;
		myStaminaRegen = 6;
		leftpunchCost = 6;
		rightpunchCost = 6;
		//.blockImpact = 1;
		//.bonusincomingDamage = 1;
		myMaxKOP = 75;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();

    }

	public void ChooseGrills (){

		//.mySpeed = 4;
		myMaxStamina = 50;
		myStaminaRegen = 5;
		leftpunchCost = 5;
		rightpunchCost = 8;
		//blockImpact = 3;
		//bonusincomingDamage = 0;
		myMaxKOP = 75;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }

	public void ChooseTizen (){

		//mySpeed = 4;
		myMaxStamina = 60;
		myStaminaRegen = 5;
		leftpunchCost = 9;
		rightpunchCost = 3;
		//blockImpact = 2;
		//bonusincomingDamage = 0;
		myMaxKOP = 67;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }
	//Heay Weight End

	//Middle Weight Start
	public void ChooseTrinity (){

		//mySpeed = 6;
		myMaxStamina = 65;
		myStaminaRegen = 6;
		leftpunchCost = 6;
		rightpunchCost = 5;
		//blockImpact = 2;
		//bonusincomingDamage = 0;
		myMaxKOP = 60;
        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }

	public void ChooseSoul (){

		//mySpeed = 6;
		myMaxStamina = 60;
		myStaminaRegen = 5;
		leftpunchCost = 4;
		rightpunchCost = 7;
		//blockImpact = 3;
		//bonusincomingDamage = 0;
		myMaxKOP = 65;
        //DecideStats ();

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
    }

	public void ChooseKaiju (){

		//mySpeed = 8;
		myMaxStamina = 55;
		myStaminaRegen = 4;
		leftpunchCost = 5;
		rightpunchCost = 5;
		//blockImpact = 2;
		//bonusincomingDamage = 2;
		myMaxKOP = 66;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }

	public void ChooseSweetness (){

		//mySpeed = 7;
		myMaxStamina = 60;
		myStaminaRegen = 5;
		leftpunchCost = 4;
		rightpunchCost = 6;
		//blockImpact = 2;
		//bonusincomingDamage = 1;
		myMaxKOP = 65;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }
	//Middle Weight End

	//Light Weight Start
	public void ChooseThemacho (){

		//mySpeed = 10;
		myMaxStamina = 70;
		myStaminaRegen = 5;
		leftpunchCost = 4;
		rightpunchCost = 4;
		//blockImpact = 2;
		//bonusincomingDamage = 0;
		myMaxKOP = 55;


        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }

	public void ChoosePerhaps (){

		//mySpeed = 9;
		myMaxStamina = 75;
		myStaminaRegen = 4;
		leftpunchCost = 3;
		rightpunchCost = 5;
		//blockImpact = 1;
		//bonusincomingDamage = 1;
		myMaxKOP = 52;


        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }

	public void ChooseKing (){

		//mySpeed = 9;
		myMaxStamina = 70;
		myStaminaRegen = 5;
		leftpunchCost = 6;
		rightpunchCost = 3;
		//blockImpact = 2;
		//bonusincomingDamage = 0;
		myMaxKOP = 55;

        SetmyCharacterValues(myMaxStamina, leftpunchCost, rightpunchCost, myMaxKOP, myStaminaRegen);
        //DecideStats ();
    }
	//Light Weight End
	//Retrive stats from other scripts
	void DecideStats(){
        Boxer = GameStateManager.gameStateManager.BoxerInput;
		Boxer.GetComponent<Stamina>().ChangeMaxandTimer (myMaxStamina, leftpunchCost, rightpunchCost, myStaminaRegen);
		Boxer.GetComponent<ANY_KOP>().PunchdamageandBonusIncoming (myMaxKOP);
		//Boxer.GetComponent<Punching_JC> ().punchSpeed = mySpeed; NOT ATTACHED TO BOXER SO IT DOESNT WORK
	}

    public void SetStatsToZero()
    {
        //CHANGES VALUES ON PREFABS BASED ON THE BOXER YOU CHOOSE
        myMaxStamina = 0;
        //mySpeed = Prefab.GetComponent<All_Characters> ().mySpeed;
        leftpunchCost = 0;
        rightpunchCost = 0;
        //blockImpact = Prefab.GetComponent<All_Characters> ().blockImpact;
        //bonusincomingDamage = Prefab.GetComponent<All_Characters> ().bonusincomingDamage;
        myMaxKOP = 0;
        myStaminaRegen = 0;
    }

    public void SetmyCharacterValues(int maxStamina, int Leftpunch, int RighPunch, int MKOP, int StamReng) {

        GameStateManager.gameStateManager.DesiredStat.myStaminaRegenToApply = StamReng;
        GameStateManager.gameStateManager.DesiredStat.LeftCostToApply = Leftpunch;
        GameStateManager.gameStateManager.DesiredStat.RightCostToApply = RighPunch;
        GameStateManager.gameStateManager.DesiredStat.myMaxKOPToApply = MKOP;
        GameStateManager.gameStateManager.DesiredStat.myStaminaRegenToApply = maxStamina;
    }

    public void ApplyMyCharacterValues() {

        GameStateManager.gameStateManager.BoxerInput.GetComponent<Stamina>().maxStamina = GameStateManager.gameStateManager.DesiredStat.myStaminaRegenToApply;
        GameStateManager.gameStateManager.BoxerInput.GetComponent<Stamina>().LeftjabCost = GameStateManager.gameStateManager.DesiredStat.LeftCostToApply;
        GameStateManager.gameStateManager.BoxerInput.GetComponent<Stamina>().RightjabCost = GameStateManager.gameStateManager.DesiredStat.RightCostToApply;
        GameStateManager.gameStateManager.BoxerInput.GetComponent<ANY_KOP>().maxPKOP = GameStateManager.gameStateManager.DesiredStat.myMaxKOPToApply;
        GameStateManager.gameStateManager.BoxerInput.GetComponent<Stamina>().maxStamTimer = GameStateManager.gameStateManager.DesiredStat.myStaminaRegenToApply;
    }

}
