﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    public GameObject MenuPanel;
    public GameObject LevelSelectPanel;

	// Use this for initialization
	void Start ()
    {
        MenuPanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ShowLevelPanel()
    {
        MenuPanel.SetActive(false);
        LevelSelectPanel.SetActive(true);
    }
    public void ShowMenuPanel()
    {
        MenuPanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
    }
    public void StartShadowBoxing()
    {
        SceneManager.LoadScene("ShadowBoxing");
    }
}
