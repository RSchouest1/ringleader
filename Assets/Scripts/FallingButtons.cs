﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingButtons : MonoBehaviour
{
    public GameObject[] ButtonR;
    public GameObject[] ButtonF;
    public GameObject[] ButtonT;
    public GameObject[] ButtonG;

    public string[] pressR;
    public string[] pressF;
    public string[] pressT;
    public string[] pressG;

    public float spawnDelay = 0.1f;

    private GameObject currentButtonObject;

    private string currentButton;

	// Use this for initialization
	void Start ()
    {
	    if(ButtonR.Length != pressR.Length)
        {
            Debug.LogError("Button Objects and Button Arrays must be the same size!");
        }

        SpawnButton();
	}

    void SpawnButton()
    {
        int randomIndex = Random.Range(0, ButtonR.Length);
        currentButtonObject = Instantiate(ButtonR[randomIndex]) as GameObject;
        currentButton = pressR[randomIndex];
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown(currentButton))
        {
            Destroy(currentButtonObject);
            Invoke("SpawnButton", spawnDelay);
        }	
	}
}
